OPS
===

OpenPolarServer Spatial Data Infrastructure

## About

The OpenPolarServer (OPS) is a complete spatial data infrastructure (SDI) built for The Center for Remote Sensing and Integrated Systems (CReSIS) at The University of Kansas. The SDI is a complete Linux server created using Rocky 9.4 and Oracle VirtualBox. The server includes the following components:

* Apache HTTPD and Apache Tomcat
* GeoServer Web Mapping Server
* PostgreSQL + PostGIS Spatial Database
* Django Web Framework
* ExtJS + GeoEXT + OpenLayers GeoPortal web application

![OPS Geoportal](images/geoportal.png)

## To cite usage of the Open Polar Server please use the following:

    CReSIS. 2024. Open Polar Server [computer software], Lawrence,  Kansas, USA. Retrieved from https://github.com/CReSIS/.

## To acknowledge the use of the CReSIS Toolbox, please note Kansas, NSF and NASA contributions. For example:

    We acknowledge the use of the CReSIS Open Polar Server from CReSIS generated with support from the University of Kansas, NASA Operation IceBridge grant NNX16AH54G, and NSF grants ICER-2126503.

## Requirements

A Rocky 9.4 Linux server or virtual machine. Rocky 9.4 ISOs available [here](https://dl.rockylinux.org/vault/rocky/9.4/isos/x86_64/) ([direct link](https://dl.rockylinux.org/vault/rocky/9.4/isos/x86_64/Rocky-9.4-x86_64-minimal.iso)). Oracle VirtualBox can be downloaded [here](https://www.virtualbox.org/wiki/Downloads).
We recommend allocating at least 8192MB of RAM to the VM, 2 processors, and 128MB of VRAM or it may freeze during setup. 80 GB for the harddrive is sufficient but more space may be needed when loading a lot of data.

## Networking
Use a NAT network adapter and add a port forwarding rule in Virtual Box under the VM's networking (expert) settings: protocol: `tcp`, host IP: `127.0.0.1`, host port: `80`, guest IP: leave blank, guest port: `80`. Add another rule for protocol: `tcp`, host IP: `127.0.0.1`, host port: `2222`, guest IP: leave blank, guest port: `22`. You will be able to access OPS from your browser at `http://127.0.0.1` once OPS setup is complete and ssh into the box at `ssh root@127.0.0.1 -p 2222` once the OS is installed.

Provisioned Virtual Machines are available at:
https://data.cresis.ku.edu/data/ops/

## Installation

On a clean Rocky 9.4 VM that you have SSH'd into from a separate terminal (Oracle VirtualBox's built-in display does not handle colored outputs and mouse-clicks very well and kernel output may clutter the UI on /dev/console):

```bash
# recommend using tmux to preserve a session in case the connection is interrupted
dnf install -y tmux
tmux

dnf install -y git
git clone https://gitlab.com/openpolarradar/ops.git /opt/ops
cd /opt/ops

sh conf/install.sh

# After the dependencies are installed, the GUI installer will automatically launch and you'll be asked questions regarding how you wish to configure OPS.

# If you cancel the installation process after the GUI has started, you can relaunch the GUI with
cd /opt/ops
source /usr/bin/venv/bin/activate
python conf/provision.py
```

OPS now has a graphical installer which guides you through the setup process
![OPS installer configuration](images/installer1.png)
![OPS installer steps](images/installer2.png)

If you run into errors during install, for the most part, the installer can offer suggested solutions. Otherwise, please open an issue.

## Endpoints

`127.0.0.1`: The geoportal for accessing OPS

`127.0.0.1/git`: A local Gitlab server with copies of a few OPS-related repos

`127.0.0.1/geoserver`: The geoserver which hosts map data

`127.0.0.1/ops`: The endpoint for reaching OPS Django views via POST

`127.0.0.1/ops/admin`: The endpoint for reaching OPS Django Admin Dashboard


### Public OPS Authentication

The username and password for the default setup are included here.

**Linux OS**

```
username: root
password: pubMaster
```

```
username: ops
password: pubOps
```

**Gitlab**

```
username: root
password: pubMaster
```

**GeoServer**

```
username: admin
password: pubAdmin
```

**PostgreSQL**

```
username: admin
password: pubAdmin
```

**OPS User**

To create a new OPS user with full permissions for use during authentication while using the toolbox:
```bash
cd /var/django/ops
# You will be prompted to set a password for the user here
python manage.py createsuperuser --username=ops --email=ops@localhost
# This user will be able to access the Django Admin dashboard at e.g. 192.168.111.222/ops/admin

# Give the user all OPS permissions
psql -U postgres -d ops -c "update opsuser_userprofile set \"layerGroupRelease\"=true, \"seasonRelease\"=true, \"createData\"=true,
 \"bulkDeleteData\"=true, \"isRoot\"=true from auth_user where user_id=auth_user.id and username = 'ops'"
```

**Toolbox**

If you use the OPR toolbox to interact with your OPS installation, make sure to edit and rerun your `startup.m` script to point the `profile(pidx).ops.url` to the address of your installation (e.g. `'http://192.168.111.222/'`).

### Adding CReSIS data to the system

This can be done by placing CReSIS "data packs" into the /data/postgresql/ directory. Information on how and where to get these data packs can be found [here.](https://gitlab.com/openpolarradar/ops/-/wikis/Data-bulkload)

## Development
The `conf/tools/build_to_live.py` script can be ran to copy updated files from the developement location to the live location.
By default, these locations are `/opt/ops/conf/django` and `/var/django/ops`, respectively.
This is where the provisions.sh script places these repositories.

After provisioning, you can develop in `/opt/ops` and commit to the git repo there and then run `build_to_live.py`
to copy the changes to the live instance at `/var/django/ops` and automatically restart `httpd`.

To attach to the live instance in VSCode, first add an entry to your .ssh/config:
```
Host ops
  Hostname 127.0.0.1
  Port 2222
  User root
```
Now install `tar` (`ssh -T ops "dnf install -y tar"`) and then connect to this entry via the VSCode remote ssh feature. Open `/opt/ops`. Install the [Python Debugger Extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python).
You can use this launch configuration (you must have `DEBUG=True` set in settings.py -- do not set this in a production environment):
```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Remote Attach",
            "type": "debugpy",
            "request": "attach",
            "connect": {
                "host": "localhost",
                "port": 65078
            },
            "pathMappings": [
                {
                    "localRoot": "/opt/ops/conf/django/ops",
                    "remoteRoot": "/var/django/ops"
                }
            ],
            "justMyCode": true
        }
    ]
}
```

The geoportal can be built by running `build_to_live.py -b`. The `-b` flag will build the geoportal and use the corresponding directories as the dev and live locations to then copy files between. Run `build_to_live.py -h` for more info.
