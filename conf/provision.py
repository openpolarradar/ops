#!/usr/bin/venv/bin/python
"""
Install and configure all necessary tooling to run and develop OPS asynchronously.

If prompt_toolkit causes the terminal to be spammed with control codes, enter "reset" to
reset the terminal.

File Author: Reece Mathews
Python Version: 3.9
"""

import asyncio
import io
import zipfile
import tarfile
import os
import pwd
import grp
import re
import sys
import traceback
import math
import time
import concurrent.futures

from functools import lru_cache, partial
from typing import Dict, Any, List, Sequence, Optional, Union
from pathlib import Path
from collections import defaultdict
from importlib import import_module
from contextlib import redirect_stdout, redirect_stderr

import yaml
import aiofiles
import aiofiles.os
import httpx
import aiologger
import aiologger.levels
import django

from install_application import create_app, set_input_widget, format_options, create_radiolist, write_output, prompt_event, create_checkboxlist, create_input, exit_, create_buttons, set_all_tasks, Progress

from aiologger.handlers.files import AsyncFileHandler
from aiologger.handlers.streams import AsyncStreamHandler
from django.utils.topological_sort import stable_topological_sort
from asgiref.sync import sync_to_async

CONFIGURATION_FILES = Path(__file__).parent/ "provisions"
TEXTBLOCKS_FILES = Path(__file__).parent / "provisions_textblocks"
FILES_FILES = Path(__file__).parent / "provisions_files"
CONFIG_PATH = Path(__file__).parent.parent / "saved_config.yaml"

COLORS = {
    "status": '\033[1;34m',  # lightblue
    'clear': '\033[0m'  # no color
}


class ArgFormatter(aiologger.logger.Formatter):
    """
    Format log output with extra arguments.
    Reference: https://stackoverflow.com/a/54593570/7587147
    """
    def format(self, record):
        # Escape format character
        record.msg = record.msg.replace("%", "%%")

        record.step_name = "Unknown"
        if record.args:
            record.step_name = record.args.get("step_name", "Unknown")
        return super().format(record)


# Setup my own logging handlers
logger = aiologger.Logger(name=__file__)
logFormatter = ArgFormatter("%(asctime)s [%(levelname)-5.5s] (%(step_name)s)  %(message)s")

# console_handler = logging.StreamHandler(sys.stdout)
# console_handler.setLevel(logging.INFO)
# console_handler.setFormatter(logFormatter)
# logger.addHandler(console_handler)

file_handler = AsyncFileHandler(filename="ops_install.log")
file_handler.formatter = logFormatter
file_handler.level = aiologger.levels.LogLevel.DEBUG
logger.add_handler(file_handler)


class LogSender:
    def __init__(self) -> None:
        pass

    def writeLog(self, msg: str) -> None:
        # using the repr of a string replaces \n with \\n. We still want the newlines so put them back
        for line in msg.replace("\\n", "\n").split("\n"):
            write_output(line)

class PromptToolkitLoghandler(AsyncStreamHandler):

    def __init__(self )-> None:
        self.sender = LogSender()
        super().__init__()

    async def emit(self, record) -> None:
        if record.levelname == "ERROR" and record.args and "step_name" in record.args:
            STEP_ERRORS[record.args['step_name']] += record.msg + "\n"
        self.sender.writeLog(self.formatter.format(record))


prompt_toolkit_handler = PromptToolkitLoghandler()
prompt_toolkit_handler.level = aiologger.levels.LogLevel.DEBUG
prompt_toolkit_handler.formatter = logFormatter
logger.add_handler(prompt_toolkit_handler)

RUNNING_CONFIG = defaultdict(dict)
STEP_ERRORS: dict[str, str] = defaultdict(lambda: "")
STEP_LOCKS = {}
CANCELED_STEPS = set()

prompt_lock = asyncio.Lock()
step_progress = Progress([], {}, {})


def init_django():
    """
    Must be ran after the app has been setup in order to access components
    of the Django app from within this script. Otherwise, django models will yield
    "ModuleNotFoundError: No module named 'ops'"
    """
    if not init_django.__dict__.get("init", False):
        logger.debug("Initializing Django", {"step_name": "django_init"})
        # Necessary to use correct shell during python_script eval
        os.chdir("/var/django/ops")
        sys.path.append('/var/django/ops')
        django.setup()

        init_django.init = True


async def get_response(title: str, prompt: str, choices: Sequence[str], radio: bool=False, step_name: str=""):
    """Ask a user for a response that matches the available choices."""
    await logger.info(f"Asking user for input: {prompt!r}", {"step_name": step_name})

    if radio:
        dialog = create_radiolist(
            title=title,
            text=prompt,
            values=[(choice, choice) for choice in choices],
            ok_text="Confirm",
        )
    else:
        dialog = create_buttons(
            title=title,
            text=prompt,
            buttons=list(choices),
        )

    chosen_option = await prompt_user(dialog, step_name)
    await logger.info(f"User selected: {chosen_option!r}", {"step_name": step_name})

    return chosen_option


async def run_command(cmd: str, step_name: str, password_in_command: bool=False, password: bool=False, return_stdout: bool=True, no_output: bool=False):
    """Run a command asynchronously and pause if an error occurs"""
    cmd_print = cmd if not password_in_command else "(omitting command containing password from log)"
    await logger.debug(f"Running command {cmd_print!r}", {"step_name": step_name})

    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate()

    command_logger = logger.debug if proc.returncode == 0 else logger.error

    await command_logger(f"Command {cmd_print!r} exited with {proc.returncode}", {"step_name": step_name})

    return_value = None
    if not no_output and stdout:
        return_value = stdout.decode().strip()
        ret_print = return_value if not password else "(omitting password from log)"
        await command_logger(f'[stdout]\n{ret_print}', {"step_name": step_name})
    if stderr:
        await logger.error(f'[stderr]\n{stderr.decode()}', {"step_name": step_name})
    if proc.returncode != 0:
        error = f"Command exited with non-zero returncode: {proc.returncode}"
        raise RuntimeError(error)

    await logger.debug("Command completed successfully", {"step_name": step_name})

    if return_stdout:
        return return_value
    else:
        return proc.returncode


def extract_each_file(obj: zipfile.ZipFile | tarfile.TarFile, path: str,
                      files: List[tuple[zipfile.ZipInfo | tarfile.TarInfo, int]],
                      step_name: str):
    """Extract an archive file by file, updating step progress as it goes."""
    total_size = sum(info[1] for info in files)
    current_size = 0
    percent = 0
    update_time = time.time()
    for file, file_size in files:
        with redirect_stdout(io.StringIO()) as f:  # this seems to print to stdout sometimes
            with redirect_stderr(io.StringIO()) as f1:
                obj.extract(file, path)

        current_size += file_size
        new_percent = int(current_size / total_size * 100)
        new_time = time.time()
        if new_percent > percent and new_time > update_time + 1:
            percent = new_percent
            update_time = new_time

            step_progress.set_step_progress_sync(step_name, new_percent)


async def extract_contents(content: bytes | io.BytesIO, path: str, filetype: Optional[str]='.zip', step_name: str=""):
    """Unzip the bytes stream in content to a file at path."""
    await logger.debug(f"Extracting content to {path!r}", {"step_name": step_name})
    step_progress.step_descriptions[step_name] = f"Extracting to {path!r}"

    if not isinstance(content, io.BytesIO):
        content = io.BytesIO(content)

    if filetype == ".zip":
        obj = zipfile.ZipFile(content)
        files = [(fileinfo, fileinfo.file_size) for fileinfo in obj.infolist()]
    elif filetype == ".tar.gz":
        obj = tarfile.open(fileobj=content)
        files = [(fileinfo, fileinfo.size) for fileinfo in obj.getmembers()]
    else:
        raise RuntimeError(f"Unknown compressed filetype: {filetype}")

    # Run in a separate thread to not lock up the GUI
    loop = asyncio.get_running_loop()
    with concurrent.futures.ThreadPoolExecutor() as pool:
        result = await loop.run_in_executor(pool, partial(extract_each_file, obj, path,
                                                          files, step_name))

    return result


def get_archive_suffix(path: Union[str, Path]):
    """Get the file extension of a compressed archive format."""
    path = str(path)
    for suffix in [".tar.gz", ".zip"]:
        if path.endswith(suffix):
            return suffix

    return None


def increment_version(version: str):
    """Increment the minor component of a version string."""
    components = version.split(".")
    try:
        # Increment the last component
        return ".".join([c for c in components[:-1]] + [str(int(components[-1]) + 1)])
    except ValueError:
        return version


async def download_file(url: str, path: str, unzip=False, step_name: str="", version=None, max_increment_attempts=10):
    """
    Asynchronously download a file from the given url to the given path.

    If increment version is true, attempt to increase the minor version number when 404s occur.
    Version number found in variable called "version".
    """
    await logger.debug(f"Downloading file at {url!r} to {path!r}" + f" version {version}" if version is not None else "", {"step_name": step_name})
    version_increment_attempts = 0
    url_template = url
    path_template = path
    while True:
        if version is not None:
            overrides = { step_name: { 'version': version } }
        else:
            overrides = None

        url = replace_vars(url_template, step_name, overrides)
        path = replace_vars(path_template, step_name, overrides)

        content = io.BytesIO()
        btyes_downloaded = 0
        percent = 0
        update_time = time.time()
        async with httpx.AsyncClient(follow_redirects=True, timeout=None) as client:
            try:
                async with client.stream('GET', url) as response:
                    if response.status_code != 200:
                        error = f"Download for file at {url!r} failed with status code {response.status_code}"
                        if version is not None and version_increment_attempts < max_increment_attempts:
                            await logger.warning(f"Download for {step_name!r} {version} gave 404. Trying version {increment_version(version)}", {"step_name": step_name})
                            version = increment_version(version)
                            step_progress.step_descriptions[step_name] = f"Attempting to increment version to {version}"
                            version_increment_attempts += 1
                            continue
                        elif version is not None:
                            # exceeded attempts
                            raise RuntimeError(f"Could not find a version for {url} that did not give a 404 after {max_increment_attempts} attempts")
                        raise RuntimeError(error)

                    total = int(response.headers["Content-Length"])
                    step_progress.step_descriptions[step_name] = f"Downloading {url} to {path}"
                    async for chunk in response.aiter_bytes():
                        content.write(chunk)
                        btyes_downloaded += len(chunk)
                        new_percent = int(btyes_downloaded / total * 100)
                        new_time = time.time()
                        if new_percent > percent and new_time > update_time + 1:
                            # Update output only if atleast one second has passed and one percent
                            percent = new_percent
                            update_time = new_time
                            await step_progress.set_step_progress(step_name, new_percent)

                content.seek(0)  # Return to beginning when done writing
                step_progress.step_descriptions[step_name] = f"Downloading to {path}"
                if version is not None:
                    if version_increment_attempts > 0:
                        await logger.warning(f"Original download for {step_name!r} gave 404. Found version {version} instead.", {"step_name": step_name})
                    RUNNING_CONFIG[step_name]['version'] = version  # update version in config incase increment was used

                if unzip:
                    await logger.debug(f"Unzipping file at {url!r} to {path!r}", {"step_name": step_name})
                    await extract_contents(content, path, get_archive_suffix(url) or ".zip", step_name)

                else:
                    async with aiofiles.open(path, "wb") as f:
                        await f.write(content.read())

                content.close()
                del content

            except Exception as e:
                error = f"File download could not be saved to path: {path!r} due to error: {e!r}"
                raise RuntimeError(error)

        return True


async def prompt_user(prompt, step_name):
    """Set up a prompt_toolkit prompt for the user and return the response."""
    async with prompt_lock:
        set_input_widget(prompt)

        await prompt_event.wait()

        if prompt_event.response == None:
            # User cancelled selection
            await logger.info("User has aborted program. Quitting.", {"step_name": step_name})
            exit_()

        return prompt_event.response


async def ask_configuration(configurations: Dict[str, Any]):
    """Ask user which OPS configuration to use."""

    options_conf = format_options(configurations["configurations"])
    chosen_conf = await prompt_user(create_radiolist(
        title="OPS Configuration",
        text="Which configuration of OPS would you like to setup?",
        values=options_conf,
        ok_text="Confirm",
    ), "configure")

    for conf in configurations["configurations"]:
        selected = chosen_conf == conf["name"]
        RUNNING_CONFIG["configurations"][conf["name"]] = selected

    return chosen_conf


async def ask_install_steps(configurations: Dict[str, Any], chosen_conf: str):
    """Ask user which install steps to perform out of optional steps."""
    install_steps = configurations["all_steps"]
    available_options = [s for s in install_steps if "optional" in s and s["optional"]]
    default_options = [c for c in configurations["configurations"] if c["name"] == chosen_conf][0]["steps"]

    options_install_steps = format_options(available_options)
    if not options_install_steps:
        return []
    chosen_steps = await prompt_user(create_checkboxlist(
        title="OPS Installation Steps",
        text="Which optional installation steps do you wish to perform?\n- Defaults selected based on chosen configuration\n- Dependencies of selected steps will be automatically ran as needed",
        values=options_install_steps,
        default_values=default_options
    ), "configure")

    return chosen_steps


async def ask_all_install_steps(all_steps: List[Dict[str, Any]]):
    """Ask user which install steps to perform out of all available steps."""
    default_options = []
    options_install_steps = format_options(all_steps)
    chosen_steps = await prompt_user(create_checkboxlist(
        title="OPS Installation Steps",
        text="[Advanced] Which installation steps do you wish to perform?\n- Dependencies must be selected manually or errors may occur\n- Step conditions will be ignored",
        values=options_install_steps,
        default_values=default_options
    ), "configure")

    return chosen_steps


def determine_dependencies(step: Dict[str, Any], install_steps: List[Dict[str, Any]], all_optional=False):
    """Get a list of all dependencies for a step including active optional-dependencies."""
    dependencies = set()

    # Only add optional dependencies to our dependency graph that have been selected
    # (they have been selected if they are in install_steps)
    for opt_dependency in set(step.get('optional-dependencies', set())):
        if get_step(opt_dependency, install_steps) is not None:
            dependencies.add(opt_dependency)

    # Regular dependencies are all added, unless all_optional is True
    # In that case, all dependencies are being manually selected
    # So only include the ones here that were selected by the user, as above
    for dependency in set(step.get('dependencies', set())):
        if not all_optional or get_step(dependency, install_steps) is not None:
            dependencies.add(dependency)

    return dependencies


def determine_install_order(install_steps: List[Dict[str, Any]], all_optional=False):
    """
    Resolve a dependency tree to determine the order in which steps must be
    performed.
    """

    dependency_graph: Dict[str, set[str]] = defaultdict(set)
    nodes = set(step["name"] for step in install_steps)

    for step in install_steps:
        dependencies = determine_dependencies(step, install_steps, all_optional)
        dependency_graph[step["name"]] = dependencies
        nodes.update(dependencies)

    order = stable_topological_sort(nodes, dependency_graph)
    return sorted(install_steps, key=lambda x: order.index(x["name"])), dependency_graph


def get_step(step_name: str, all_steps: List[Dict[str, Any]]):
    """Find a step's configuration by name."""
    for step in all_steps:
        if step['name'] == step_name:
            return step

    return None


@lru_cache()
def get_descendents(step_name: str):
    """
    Get all steps which have the given step as a dependency somewhere up the chain.
    Stops at optional steps which have not been selected for install.
    Checks to make sure there are no unselected optional steps in the ancestors as well.
    """
    dependents: set[str] = set()
    for step in get_descendents.all_steps:
        if step_name in step.get('dependencies', []) and step['name'] not in dependents:
            if not step.get("optional", False) or step in get_descendents.install_steps:
                # Only add the step if it is not optional or it is already determined for install

                # Check that all of this steps dependencies will be met
                ancestors = get_ancestors(step['name'])
                for ancestor_name in ancestors:
                    ancestor_step = get_step(ancestor_name, get_descendents.all_steps)
                    if ancestor_step.get("optional", False) and ancestor_step not in get_descendents.install_steps:
                        # This depescendent step is also descendent from an unselected optional step.
                        # It should not be installed then as one of it's dependencies will not be met
                        break
                else:
                    # All dependencies met, safe to install
                    dependents.add(step['name'])
                    dependents |= get_descendents(step['name'])

    return dependents


@lru_cache()
def get_ancestors(step_name: str):
    """
    Traverse up the dependency chain for the given step. Only has to iterate
    the list of dependencies for each dependency.
    """
    step = get_step(step_name, get_ancestors.all_steps)
    if not step:
        raise ValueError(f"Step not found: {step_name}")

    ancestors = set()
    for dependency in step.get("dependencies", []):
        ancestors.add(dependency)
        ancestors |= get_ancestors(dependency)

    return ancestors


async def determine_install_steps(configurations: Dict[str, Any], chosen_conf: str,
                                  chosen_steps: List[str]):
    """
    Determine which steps will be performed during install and accumulate a list of
    packages that must be installed beforehand.
    """

    install_steps: List[Dict[str, Any]] = []

    # Step 1. Add all non-optional steps that have no dependencies
    for step in configurations["all_steps"]:
        if not step.get("optional", False) and not step.get("dependencies", []):
            install_steps.append(step)

    get_ancestors.all_steps = configurations["all_steps"]

    # Step 2. Add all user-selected optional steps
    for step_name in chosen_steps:
        step = get_step(step_name, configurations["all_steps"])
        install_steps.append(step)

        # Step 3. Add all optional ancestors of user-selected optional steps
        for ancestor_name in get_ancestors(step_name):
            step_ = get_step(ancestor_name, configurations["all_steps"])
            assert step_ is not None
            if step_.get("optional", False) and step_ not in install_steps:
                await logger.info(f"Optional step '{ancestor_name}' was not selected but has been marked for install because optional selected step '{step_name}' depends on it.", {"step_name": "determine_install_steps"})
                install_steps.append(step_)

    get_descendents.all_steps = configurations["all_steps"]
    get_descendents.install_steps = install_steps

    # Step 4. Go down the dependents of all steps that have been determined for install
    #         and add all non-optional steps that do not have an unselected optional ancestor
    for step in install_steps:
        descendents = get_descendents(step['name'])
        for descendent_name in descendents:
            step_ = get_step(descendent_name, configurations["all_steps"])

            if not step_.get("optional", False) and step_ not in install_steps:
                install_steps.append(step_)

    return install_steps


def find_variables_in_config(step_name: str, config: Any, all_steps: List[Dict[str, Any]], text_blocks: Dict[str, Dict[str, str]], include_confirms: bool = False) -> set[tuple[str, str]]:
    """
    Recursively check each component of a config for config variables.
    """
    step_variables: set[tuple[str, str]] = set()

    if isinstance(config, list):
        for conf in config:
            step_variables |= find_variables_in_config(step_name, conf, all_steps, text_blocks, include_confirms)
    elif isinstance(config, dict):
        if include_confirms and config.get('response-type', '') == 'confirm':
            # usually only keep configs that get referenced somewhere but confirmation configs should be displayed regardless
            step_variables.add((step_name, config['name']))
            return step_variables
        for attr, value in config.items():
            if attr.endswith("-block"):
                if step_name not in text_blocks or value not in text_blocks[step_name]:
                    raise ValueError(f"Step {step_name} mentions a value '{value}' in the yaml textblocks file but no such value is present")
                value = text_blocks[step_name][value]
            elif attr.endswith("-file"):
                if value not in os.listdir(FILES_FILES):
                    raise FileNotFoundError(f"Step {step_name} mentions a file '{value}' in the provisions_files directory but no such file is present")
                with open(FILES_FILES / value) as f:
                    value = f.read()
            elif attr == "action" and value in ["run_step", "run_solution"]:
                # TODO: could narrow down run_solution search to just that solution
                run_step_name = config.get("step", None)
                if not run_step_name:
                    raise ValueError(f"Step {step_name} has run_step action configured but no step key")

                step_variables |= find_variables_in_config(run_step_name, get_step(run_step_name, all_steps), all_steps, text_blocks, include_confirms)
                return step_variables

            step_variables |= find_variables_in_config(step_name, value, all_steps, text_blocks, include_confirms)
    elif isinstance(config, str):
        variable_matches = re.findall(r"\$\{([a-zA-Z_0-9]+:)?([a-zA-Z_0-9]+)\}", config)
        for variable_match in variable_matches:
            if variable_match[0].strip(":") in ['configurations']:
                # skip special variables
                continue
            if not variable_match[0].strip(":"):
                # referencing self
                step_variables.add((step_name, variable_match[1]))
            else:
                step_variables.add((variable_match[0].strip(":"), variable_match[1]))

    return step_variables


def determine_config_dependencies(install_steps: List[Dict[str, Any]], all_steps: List[Dict[str, Any]], text_blocks: Dict[str, Dict[str, str]]):
    """
    Find the config dependencies in a given step.
    """

    # Find initial set of variables in all fields on install steps
    dependency_graph = defaultdict(set)
    to_search = set()
    for step in install_steps:
        # search all fields in step
        new_vars = find_variables_in_config(step['name'], step, all_steps, text_blocks, include_confirms=True)
        for var in new_vars:
            if var not in dependency_graph:
                dependency_graph[var] = set()
        to_search |= new_vars

    # Then find all configs referenced in the configs we've already found
    found_vars = set()
    while to_search - found_vars:
        variable = list(to_search - found_vars)[0]

        for conf in get_step(variable[0], all_steps).get("configs", []):
            if conf['name'] == variable[1]:
                # specifically only search the given config
                new_vars = find_variables_in_config(variable[0], conf, all_steps, text_blocks, include_confirms=False)
                dependency_graph[variable] |= new_vars
                to_search |= new_vars
                break
        found_vars.add(variable)

    return dependency_graph


def find_config_params(step_name: str, config_name: str, all_steps: List[Dict[str, Any]]):
    """
    Find a specific config param dict by step name and config name.
    """
    step = get_step(step_name, all_steps)
    for conf in step.get("configs", []):
        if conf.get("name", None) == config_name:
            # if provided config_name is "" then we will run all configs for this step
            return conf

    raise ValueError(f"{step_name}:{config_name} config not found but referenced in another step.")


async def determine_config_order(install_steps: List[Dict[str, Any]], all_steps: List[Dict[str, Any]], text_blocks: Dict[str, Dict[str, str]]):
    """
    Determine which steps need to be configured by building a dependency graph
    of config variables.
    """

    dependency_graph = determine_config_dependencies(install_steps, all_steps, text_blocks)
    nodes = set(dependency_graph)

    order = stable_topological_sort(nodes, dependency_graph)

    return [(step_name, find_config_params(step_name, config_name, all_steps)) for (step_name, config_name) in order]


def replace_vars(string: str, step_name: str=None, overrides: Optional[dict]=None):
    """
    Replace the variable designations in the string that are in the form of
    ${config_name:config_item} with their corresponding value in RUNNING_CONFIG
    that was assigned by the user during the configuration step.

    overrides is a dict of more variables to swap out.
    """
    if not overrides:
        overrides = {}

    string = str(string)
    for step, variables in list(overrides.items()) + list(RUNNING_CONFIG.items()):
        for var, value in variables.items():
            variable_key = r"${%s:%s}" % (step, var)
            string = string.replace(variable_key, str(value))

            if step == step_name:
                variable_key = r"${%s}" % var
                string = string.replace(variable_key, str(value))

    # double check that there aren't any remaining variables that might have been missed
    if matches := re.findall(r"\$\{\S*\}", string):
        logger.warning(f"Config variable {matches[0]} not found in runnning config", {"step_name": step_name})

    return string


async def get_default(step_name: str, config_parameter: Dict[str, Any]):
    """Determine the default option for a given configuration step."""
    await logger.debug(f"Determining default value for {step_name}:{config_parameter['name']}", {"step_name": "config_" + step_name})

    password_in_command = config_parameter.get("password_in_command", False)
    password = config_parameter.get("password", False)
    if "default-cmd" in config_parameter:
        return await run_command(replace_vars(config_parameter["default-cmd"], step_name),
                                 "config_" + step_name, password_in_command, password)
    elif "default-py" in config_parameter:
        for lib in config_parameter.get("py-modules", []):
            if lib not in globals():
                globals()[lib] = import_module(lib)
        return eval(replace_vars(config_parameter['default-py'], step_name))
    elif "default" in config_parameter:
        default = config_parameter["default"]
        default = replace_vars(default, step_name)

        return default

    return None


async def ask_config_step(step_name: str, config_parameters: Dict[str, Any], accept_defaults: bool):
    """
    Present the user with a configuration question for a single installation step.
    """
    await logger.debug(f"Presenting configuration {config_parameters['name']!r} for step: {step_name!r}", {"step_name": "config_" + step_name})
    config_name = config_parameters["name"]

    default = await get_default(step_name, config_parameters)

    prompt = config_parameters.get("prompt", "")
    if default is None:
        default = ""

    response = None
    response_type = config_parameters.get("response-type", "none")
    if accept_defaults:
        await logger.debug(f"Skipping prompt for {step_name}:{config_name} since user chose to accept defaults", {"step_name": "config_" + step_name})
        response = default
    elif response_type == "free":
        response = await prompt_user(create_input(
            title=f"{step_name}:{config_name} Configuration",
            default=default,
            password=config_parameters.get("password", False),
            text=prompt,
        ), step_name)
    elif response_type == "confirm":
        response = await get_response(
            title=f"{step_name}:{config_name} Configuration",
            prompt=prompt,
            choices=("Continue", ),
            step_name=step_name
        )
    elif response_type == "yes/no":
        response = await get_response(
            title=f"{step_name}:{config_name} Configuration",
            prompt=prompt,
            choices=("Yes", "No", ),
            step_name=step_name
        ) == "Yes"
    elif response_type == "none":
        await logger.debug(f"Skipping prompt for {step_name}:{config_name} since response-type is none", {"step_name": "config_" + step_name})
        response = default

    RUNNING_CONFIG[step_name][config_name] = response

    if config_parameters.get("password", False):
        await logger.info(f"Configuration for {step_name}:{config_name} set (omitting password from log)", {"step_name": "config_" + step_name})
    else:
        await logger.info(f"Configuration for {step_name}:{config_name} set to {str(response).strip()!r}", {"step_name": "config_" + step_name})


async def perform_config_steps(config_steps: List[tuple[str, Dict[str, Any]]], accept_defaults: bool):
    """
    Present the user with configuration options for each of the steps that
    will be completed before beginning the installation.
    """
    await step_progress.set_step_state("Configure Steps (synchronous)", "[IN PROGRESS]", logger=logger)

    for step_name, config_parameters in config_steps:
        await ask_config_step(step_name, config_parameters, accept_defaults)


async def load_saved_config(config_steps: List[tuple[str, Dict[str, Any]]]):
    """Attempt to load a saved config into RUNNING_CONFIG."""
    await logger.info(f"Loading user config from {CONFIG_PATH}", {"step_name": 'configure'})
    try:
        async with aiofiles.open(CONFIG_PATH) as f:
            content = await f.read()
            config = yaml.load(content, Loader=yaml.Loader)
    except (FileNotFoundError, yaml.YAMLError):
        return None, None

    if not content or not config:
        return None, None

    missing_configs = []
    for step_name, config_parameters in config_steps:
        if step_name not in config or config_parameters['name'] not in config[step_name]:
            missing_configs.append((step_name, config_parameters))

    return config, missing_configs


def strip_passwords(configs, all_steps):
    """Remove passwords from the configs."""
    stripped_configs = defaultdict(dict)
    for step_name, config in configs.items():
        if step_name == "configurations":
            continue
        for config_name, value in config.items():
            if find_config_params(step_name, config_name, all_steps).get("password", False):
                continue
            stripped_configs[step_name][config_name] = value

    return stripped_configs


async def save_config(all_steps):
    """Save RUNNING_CONFIG to CONFIG_PATH."""
    await logger.info(f"Saving user config to {CONFIG_PATH}", {"step_name": 'configure'})
    async with aiofiles.open(CONFIG_PATH, "w") as f:
        config = yaml.dump(strip_passwords(RUNNING_CONFIG, all_steps), Dumper=yaml.Dumper)
        await f.write(config)


async def perform_install_packages(install_packages: List[str]):
    """Install all the DNF packages which are required for the chosen configuration."""
    step_name = "install_packages"
    await logger.info("Updating all packages", {"step_name": step_name})
    await step_progress.set_step_state("Install Packages (synchronous)", "[IN PROGRESS]", logger=logger)

    await run_command("dnf update -y", step_name)

    if install_packages:
        await logger.info("Installing package dependencies", {"step_name": step_name})
        install_packages_str = replace_vars(" ".join(install_packages), step_name)
        await run_command("dnf install -y " + install_packages_str, step_name)
    else:
        await logger.info("No package dependencies to install", {"step_name": step_name})

    await step_progress.set_step_state("Install Packages (synchronous)", "[COMPLETE   ]", logger=logger)


async def perform_install_pip(install_pip: List[str]):
    """Install all the pip packages which are required for the chosen configuration."""
    step_name = "install_pip_packages"

    await logger.info("Updating pip", {"step_name": step_name})
    await step_progress.set_step_state("Install Pip Packages (synchronous)", "[IN PROGRESS]", logger=logger)


    await run_command("python -m pip install --upgrade pip", step_name)

    if install_pip:
        await logger.info("Installing pip dependencies", {"step_name": step_name})
        install_packages_str = replace_vars(" ".join(install_pip), step_name)
        await run_command("pip install --upgrade " + install_packages_str, step_name)
    else:
        await logger.info("No pip dependencies to install", {"step_name": step_name})

    await step_progress.set_step_state("Install Pip Packages (synchronous)", "[COMPLETE   ]", logger=logger)


def get_text(step_name: str, action_parameters: Dict[str, Any],
             text_blocks: Dict[str, Dict[str, str]], prefix=""):
    """Get the text associated with an action."""
    if prefix + "text" in action_parameters:
        text = action_parameters[prefix + "text"]
    elif prefix + "text-block" in action_parameters:
        block_name = action_parameters[prefix + 'text-block']
        text = text_blocks[step_name][block_name]
    elif prefix + "text-file" in action_parameters:
        file_name = action_parameters[prefix + 'text-file']
        if file_name not in os.listdir(FILES_FILES):
            raise FileNotFoundError(f"Step {step_name} mentions a file '{file_name}' in the provisions_files directory but no such file is present")
        with open(FILES_FILES / file_name) as f:
            text = f.read()
    else:
        raise ValueError(f"Either 'text' or 'text-block parameter expected in provisions.yaml with file actions for step {step_name}")

    return replace_vars(text, step_name)


def get_line_components(line: str, comment_char="#", assignment_char="=", normalize_strings=False):
    """Break a line from a conf file into its component parts."""
    line_formatted = line.strip().lstrip(comment_char).strip()  # Remove comment if line starts with one
    line_content, *line_comment = line_formatted.split(comment_char)  # Get comment from end of line
    line_comment = comment_char.join(line_comment).strip()
    if line_comment:
        line_comment = f" {comment_char} {line_comment}"

    line_key, *line_value = line_content.split(assignment_char)
    line_key = line_key.strip()
    line_value = assignment_char.join(line_value).strip()

    if line.lstrip().startswith(comment_char):
        comment = comment_char
    else:
        comment = ""

    line_key = " ".join(line_key.split())
    line_value = " ".join(line_value.split())

    if normalize_strings:
        # TODO: is there a better way to find strings in a key and replace " with '?
        line_key = line_key.replace('"', "'")

    indent = re.match(r"^\s*", line)
    if indent:
        indent = indent[0]
    else:
        indent = ""

    return line_key, line_value, line_comment, comment, indent


async def update_conf(step_name: str, action_parameters: Dict[str, Any],
                      text_blocks: Dict[str, Dict[str, str]]):
    """Update a conf file line by line with the values given in text."""
    path = replace_vars(action_parameters["file_path"], step_name)
    text = get_text(step_name, action_parameters, text_blocks)

    comment_char = action_parameters.get("comment-char", "#")
    assignment_char = action_parameters.get("assignment-char", "=")
    normalize_strings = action_parameters.get("normalize-strings", False)

    await logger.debug(f"Finding updated lines in conf file at {path!r}", {"step_name": step_name})

    async with aiofiles.open(path) as f:
        content = await f.read()

    # get all new keys
    all_keys = {}
    for update_line in text.split("\n"):
        if not update_line.strip():
            continue
        new_key, new_value, new_comment, new_commented, indent = get_line_components(update_line, comment_char, assignment_char, normalize_strings)
        all_keys[new_key] = new_commented + indent + new_key + assignment_char + new_value + new_comment

    # Iterate each line of the current conf file looking for keys
    found_keys = set()
    new_lines = []
    for line in content.split("\n"):
        if assignment_char not in line:
            new_lines.append(line)
            continue
        line_key, _, line_comment, _, indent = get_line_components(line, comment_char, assignment_char, normalize_strings)

        # Iterate each line in the passed-in text and see if any of its keys match the current key
        for update_line in text.split("\n"):

            new_key, new_value, new_comment, new_commented, _ = get_line_components(update_line, comment_char, assignment_char, normalize_strings)
            if not new_comment:
                new_comment = line_comment

            if line_key == new_key:

                # Add update statement if not already present
                found_keys.add(new_key)
                if action_parameters.get('comment-updates', False):
                    update_comment = f"{comment_char} THIS LINE HAS BEEN UPDATED BY provision.py:"
                    if not new_lines or new_lines[-1] != update_comment:
                        new_lines.append(update_comment)

                new_lines.append(new_commented + indent + new_key + assignment_char + new_value + new_comment)
                break
        else:
            new_lines.append(line)

    remaining_keys = all_keys.keys() - found_keys
    if remaining_keys and action_parameters.get('comment-updates', False):
        new_lines.append("# THE FOLLOWING LINES WERE ADDED BY provision.py:")

    new_lines.extend([all_keys[k] for k in remaining_keys])

    await logger.debug(f"Updating conf file at {path!r}", {"step_name": step_name})
    async with aiofiles.open(path, "w") as f:
        await f.write("\n".join(new_lines))


async def replace_lines(step_name: str, action_parameters: Dict[str, Any],
                        text_blocks: Dict[str, Dict[str, str]]):
    """
    Iterate each line in old-text and find matching lines in file_path. Replace the
    lines with the corresponding line in new-text.
    """
    path = replace_vars(action_parameters["file_path"], step_name)
    old_text = get_text(step_name, action_parameters, text_blocks, "old-")
    new_text = get_text(step_name, action_parameters, text_blocks, "new-")

    comment_char = action_parameters.get("comment-char", "#")
    assignment_char = action_parameters.get("assignment-char", "=")
    normalize_strings = action_parameters.get("normalize-strings", False)

    async with aiofiles.open(path) as f:
        content = await f.read()

    new_lines = []
    for current_line in content.split("\n"):
        current_key, _, current_comment, _, indent = get_line_components(current_line, comment_char, assignment_char, normalize_strings)

        for old_line, new_line in zip(old_text.split("\n"), new_text.split("\n")):
            old_key, _, _, _, _ = get_line_components(old_line, comment_char, assignment_char, normalize_strings)
            new_key, _, new_comment, new_commented, _ = get_line_components(new_line, comment_char, assignment_char, normalize_strings)

            if not new_comment:
                new_comment = current_comment

            if current_key == old_key:

                # Add update statement if not already present
                if action_parameters.get('comment-updates', False):
                    update_comment = f"{comment_char} THIS LINE HAS BEEN UPDATED BY provision.py:"
                    if not new_lines or new_lines[-1] != update_comment:
                        new_lines.append(update_comment)

                new_lines.append(new_commented + indent + new_key + new_comment)
                break
        else:
            new_lines.append(current_line)

    await logger.debug(f"Updating file at {path!r}", {"step_name": step_name})
    async with aiofiles.open(path, "w") as f:
        await f.write("\n".join(new_lines))


async def update_file(step_name: str, action_parameters: Dict[str, Any],
                      text_blocks: Dict[str, Dict[str, str]]):
    action = action_parameters['action']

    path = replace_vars(action_parameters["file_path"], step_name)
    text = get_text(step_name, action_parameters, text_blocks)
    text_log = text if not action_parameters.get("password", False) else "(omitting password from log)"

    if action == "append_file":
        await logger.debug(f"Appending to {path!r}: {text_log!r}", {"step_name": step_name})
        file_mode = "a"
        if action_parameters.get("check", True):
            async with aiofiles.open(path, "r") as f:
                if text in await f.read():
                    await logger.debug("File already contains the text to append", {"step_name": step_name})
                    return
    else:
        assert action == "write_file"
        await logger.debug(f"Writing to {path!r}: {text_log!r}", {"step_name": step_name})
        file_mode = "w"

    async with aiofiles.open(path, file_mode) as f:
        newline = '\n' if file_mode == "a" else ""
        await f.write(newline + text)


async def perform_install_step(step: Dict[str, Any],
                               text_blocks: Dict[str, Dict[str, str]],
                               install_steps: List[Dict[str, Any]],
                               all_steps: List[Dict[str, Any]],
                               retry: bool=False, nodisplay: bool=False, all_optional=False):
    """Perform a single installation step"""

    if "condition" in step and not all_optional:
        # Skip conditions if in manual config (all_optional set when in manual config)
        condition = replace_vars(step['condition'], step['name'])

        if condition.title() != "True":
            msg = "Skipping step due to condition being false"
            await logger.info(msg, {"step_name": step['name']})
            STEP_ERRORS[step['name']] += msg + "\n"

            if "condition-msg" in step:
                msg = replace_vars(step['condition-msg'], step['name'])
                step_progress.step_descriptions[step['name']] = msg
                STEP_ERRORS[step['name']] += msg + "\n"
                await logger.info(msg, {"step_name": step['name']})

            if not nodisplay:
                await step_progress.set_step_state(step['name'], '[SKIPPED    ]', logger=logger)

            STEP_LOCKS[step['name']].set()
            return

    if not retry and not nodisplay:
        await step_progress.set_step_state(step['name'], '[WAITING    ]', logger=logger)

        for dependency in determine_dependencies(step, install_steps, all_optional):
            await STEP_LOCKS[dependency].wait()

            if dependency in CANCELED_STEPS:
                # canceled steps have all dependents canceled as well
                CANCELED_STEPS.add(step['name'])
                await step_progress.set_step_state(step['name'], '[CANCELLED  ]', logger=logger)
                await logger.info("Cancelling step due to parent step canceled", {"step_name": step['name']})
                STEP_LOCKS[step['name']].set()
                return

    if not nodisplay:
        await step_progress.set_step_state(step['name'], '[IN PROGRESS]', logger=logger)

    description = ": " + step["description"] if "description" in step else ""
    await logger.info(f"Performing step: {step['name']!r}" + description, {"step_name": step['name']})

    for action_parameters in step["actions"]:
        try:
            action = action_parameters["action"]
            if action == "commands":
                for command in action_parameters["commands"]:
                    command = replace_vars(command, step['name'])
                    await run_command(command, step['name'], action_parameters.get("password_in_command", False),
                                      action_parameters.get("password", False), no_output=action_parameters.get("no_output", False))
                    await asyncio.sleep(0)
            elif action == "download":
                version = replace_vars(action_parameters.get("version", None), step_name=step['name'])
                if version == "None":
                    version = None
                unzip = action_parameters["unzip"] if "unzip" in action_parameters else False
                await download_file(action_parameters["url"], action_parameters["output_path"], unzip=unzip,
                                    step_name=step['name'], version=version,
                                    max_increment_attempts=action_parameters.get("max_increment_attempts", 10))
            elif action == "unzip_all":
                # TODO[reece]: Make these all occur asynchronously -- create tasks for each and then gather?
                source_dir = Path(replace_vars(action_parameters['source_dir'], step['name']))
                target_dir = replace_vars(action_parameters['target_dir'], step['name'])
                count = 0
                for path in source_dir.iterdir():
                    suffix = get_archive_suffix(path)
                    if suffix is not None:
                        count += 1
                        await logger.debug(f"Extracting '{path}' to '{target_dir}'", {"step_name": step['name']})
                        async with aiofiles.open(path, "rb") as f:
                            content = await f.read()  # TODO: Can the file be passed to extract instead?

                        await extract_contents(content, target_dir, filetype=suffix, step_name=step['name'])
                if count == 0:
                    raise RuntimeError("No files found to unzip")
            elif action == "set_env":
                value = replace_vars(action_parameters["value"], step['name'])
                file_value = value
                variable = action_parameters["variable"]
                prepend = action_parameters.get("prepend", False)
                await logger.debug(f"Setting environment variable {variable!r}={file_value!r}", {"step_name": step['name']})

                if prepend:
                    file_value = f"{value}:${variable}"

                    # set value in current environment
                    value_parts = os.environ.get(variable, "").split(":")

                    # remove value from existing path if already in path
                    if value in value_parts:
                        value_parts.remove(value)

                    # add to front of path
                    value_parts.insert(0, value)

                    os.environ[variable] = ":".join(value_parts)
                else:
                    os.environ[variable] = value

                env_params = {
                    "action": "append_file",
                    "file_path": str(Path.home() / ".bashrc"),
                    "text": f"export {variable}={file_value}"
                }
                await update_file(step['name'], env_params, text_blocks)

            elif action == "make_dirs":
                permissions = int(str(action_parameters.get("permissions", "0o511")), base=8)
                owner = action_parameters.get("owner", -1)
                group = action_parameters.get("group", -1)
                if owner != -1:
                    owner = pwd.getpwnam(owner).pw_uid
                if group != -1:
                    group = grp.getgrnam(group).gr_gid

                for directory in action_parameters["dirs"]:
                    directory = replace_vars(directory, step['name'])
                    await logger.debug(f"Making directory {directory!r} with permissions {permissions:o}", {"step_name": step['name']})
                    await aiofiles.os.makedirs(directory, mode=permissions, exist_ok=True)
                    if owner != -1 or group != -1:
                        os.chown(directory, owner, group)
            elif action == "append_file" or action == "write_file":
                await update_file(step['name'], action_parameters, text_blocks)
            elif action == "update_conf":
                await update_conf(step['name'], action_parameters, text_blocks)
            elif action == "replace_lines":
                await replace_lines(step['name'], action_parameters, text_blocks)
            elif action == "python_script":
                init_django()
                try:
                    content = get_text(step['name'], action_parameters, text_blocks)
                except ValueError:
                    # If no text or text-block attribute, try reading the script at "path"
                    path = replace_vars(action_parameters['path'], step['name'])
                    async with aiofiles.open(path, "r") as f:
                        content = await f.read()

                logger.debug(f"Running code: `{content}`", {"step_name": step['name']})

                loop = asyncio.get_running_loop()
                coros = []  # this list can be populated from within exec. the coros within will be ran after exec in this event loop
                with redirect_stdout(io.StringIO()) as f:
                    with redirect_stderr(io.StringIO()) as f1:
                        await sync_to_async(lambda: exec(replace_vars(content, step['name']), globals(), {'loop': loop, 'coros': coros}))()

                        # Run any corotines returned from the exec
                        for coro in coros:
                            await coro

                        stdout = f.read().strip()
                        stderr = f1.read().strip()
                        if stdout:
                            await logger.debug(f'[stdout from python_script]\n{stdout}', {"step_name": step['name']})
                        if stderr:
                            await logger.error(f'[stderr from python_script]\n{stderr}', {"step_name": step['name']})
            elif action == "run_step":
                new_step_name = action_parameters['step']
                new_step = get_step(new_step_name, all_steps)
                logger.debug(f"Step {step['name']} is running step {new_step_name}", {"step_name": step['name']})
                await perform_install_step(new_step, text_blocks, install_steps, all_steps, False, True, all_optional)
            elif action == "run_solution":
                new_step_name = action_parameters['step']
                new_step = get_step(new_step_name, all_steps)
                new_solution_name = action_parameters['solution']
                new_solution = None

                for new_action in new_step['actions']:
                    for solution_ in new_action.get("solutions", []):
                        if solution_['name'] == new_solution_name:
                            new_solution = solution_
                            break
                    if new_solution:
                        break
                else:
                    # Matching solution not found
                    raise ValueError(f"Solution {new_solution_name} does not exist in step {new_step_name}")

                await perform_install_step(new_solution, text_blocks, install_steps, all_steps, True, True, all_optional)
            elif action == "set_description":
                step_progress.step_descriptions[step["name"]] = replace_vars(action_parameters["description"], step['name'])
            else:
                raise ValueError(f"Action type {action} is unknown")

        except Exception as e:
            # Ask user how to proceed when an error occurs
            await logger.error("Error occurred during step execution", {"step_name": step['name']})
            await logger.error(str(e), {"step_name": step['name']})
            await logger.debug(traceback.format_exc(), {"step_name": step['name']})

            options = ["retry", "skip"]
            if not nodisplay:
                # steps that have dependents let user choose to skip dependents
                options = ["retry", "cancel (also cancels dependent steps)", "skip (still runs dependent steps)"]
                await step_progress.set_step_state(step["name"], "[ERROR      ]", logger=logger)

            solutions = action_parameters.get("solutions", [])
            solution_map = {replace_vars(solution['name'], step['name']): solution for solution in solutions}
            options = list(solution_map.keys()) + options

            response = await get_response(f"{step['name']} Failed",
                                          STEP_ERRORS[step['name']],
                                          options, radio=True, step_name=step['name'])
            if not nodisplay:
                step_progress.step_descriptions[step["name"]] = f"trying '{response}'"
                await step_progress.set_step_state(step["name"], "[RESOLVING  ]", logger=logger)
            if response == 'retry':
                return await perform_install_step(step, text_blocks, install_steps, all_steps, True, nodisplay, all_optional)

            elif response.startswith('skip'):
                await logger.info("Skipping action", {"step_name": step['name']})

                if not nodisplay:
                    await step_progress.set_step_state(step["name"], "[SKIPPED    ]", logger=logger)
                    STEP_LOCKS[step['name']].set()

                return None

            elif response.startswith('cancel'):
                await logger.info("Cancelling action", {"step_name": step['name']})

                if not nodisplay:
                    await step_progress.set_step_state(step["name"], "[CANCELLED  ]", logger=logger)
                    CANCELED_STEPS.add(step['name'])
                    STEP_LOCKS[step['name']].set()

                return None

            else:
                assert response in solution_map
                solution = solution_map[response]

                await perform_install_step(solution, text_blocks, install_steps, all_steps, True, True, all_optional)
                if not solution.get("no_rerun", False):
                    return await perform_install_step(step, text_blocks, install_steps, all_steps, True, nodisplay, all_optional)

    if not nodisplay:
        if retry:
            await logger.debug("The chosen solution appears to have resolved the issue", {"step_name": step['name']})
        await step_progress.set_step_state(step["name"], "[COMPLETE   ]", logger=logger)
        STEP_LOCKS[step['name']].set()


async def perform_install_steps(install_steps: List[Dict[str, Any]],
                                dependency_graph: Dict[str, set[str]],
                                text_blocks: Dict[str, Dict[str, str]],
                                all_steps: List[Dict[str, Any]], all_optional: bool):
    """Perform the installation based on the steps the user selected."""
    all_steps_coros = (perform_install_step(step, text_blocks, install_steps, all_steps, False, False, all_optional) for step in install_steps
                 if "actions" in step)

    await asyncio.gather(*all_steps_coros)


def determine_install_packages(install_steps):
    """Determine which packages need to be installed for the given steps."""
    install_packages = set()
    install_pip = set()

    # Gather all package and pip dependencies for the determined steps
    for step in install_steps:
        if "package-dependencies" in step:
            install_packages |= set(step["package-dependencies"])
        if "pip-dependencies" in step:
            install_pip |= set(step["pip-dependencies"])

    return install_packages, install_pip


async def read_all_provisions():
    """Read every yaml file in the provisions directory into a dict."""
    configurations = defaultdict(list)

    for file in sorted(CONFIGURATION_FILES.iterdir(), reverse=True):
        async with aiofiles.open(file) as f:
            content = await f.read()
            new_configs = yaml.load(content, Loader=yaml.Loader)
            configurations['configurations'].extend(new_configs['configurations'])
            configurations['all_steps'].extend(new_configs['all_steps'])

    # only keep last steps of the same name (filenames sorted Z-A)
    all_steps = {}
    for step in configurations['all_steps']:
        all_steps[step['name']] = step

    configurations['all_steps'] = list(all_steps.values())

    return configurations


async def read_all_provision_textblocks():
    """Read every yaml file in the provisions TEXTBLOCKS_FILES directory into a dict."""
    text_blocks = defaultdict(dict)

    for file in sorted(TEXTBLOCKS_FILES.iterdir(), reverse=True):
        async with aiofiles.open(file) as f:
            content = await f.read()
            new_texts = yaml.load(content, Loader=yaml.Loader)
            text_blocks.update(new_texts)

    return text_blocks


async def install():
    """Run through all steps to install OPS."""
    global step_progress, RUNNING_CONFIG

    configurations = await read_all_provisions()
    text_blocks = await read_all_provision_textblocks()

    await logger.info("Beginning OPS install configuration", {"step_name": "begin_install"})

    chosen_conf = await ask_configuration(configurations)

    if chosen_conf != "manual":
        chosen_steps = await ask_install_steps(configurations, chosen_conf)
    else:
        chosen_steps = await ask_all_install_steps(configurations["all_steps"])

    install_steps = await determine_install_steps(configurations, chosen_conf, chosen_steps)
    install_steps, dependency_graph = determine_install_order(install_steps, all_optional=chosen_conf=="manual")

    if chosen_conf == "manual":
        # now that we have the dependency graph (with all steps optional), we can narrow down to our chosen steps
        install_steps = [get_step(step_name, configurations["all_steps"]) for step_name in chosen_steps]

    config_steps = await determine_config_order(install_steps, configurations['all_steps'], text_blocks)

    install_packages, install_pip = determine_install_packages(install_steps)

    step_order = [s['name'] for s in install_steps if 'actions' in s]
    step_descriptions = defaultdict(lambda: "")
    step_descriptions.update({s['name']: s.get('description', '') for s in install_steps
                              if 'actions' in s})
    for step in step_order:
        STEP_LOCKS[step] = asyncio.Event()

    extra_steps = ['Install Packages (synchronous)', 'Install Pip Packages (synchronous)', 'Configure Steps (synchronous)']
    step_progress = Progress(extra_steps + step_order, step_descriptions, dependency_graph)

    saved_config, missing_configs = await load_saved_config(config_steps)

    response = await get_response(
            title=f"OPS Configuration",
            prompt="Would you like to configure OPS (recommended) or skip configuration and accept all default values?",
            choices=("Configure", "Skip") + (("Load Saved",) if saved_config is not None else tuple()),
            step_name="configure_ops"
        )

    accept_defaults = response == "Skip"

    if response == "Load Saved":
        RUNNING_CONFIG.update(saved_config)
        config_steps = missing_configs

    await perform_install_packages(install_packages)
    await perform_install_pip(install_pip)

    await perform_config_steps(config_steps, accept_defaults)

    await save_config(configurations['all_steps'])

    await step_progress.set_step_state("Configure Steps (synchronous)", "[COMPLETE   ]", logger=logger)

    response = await get_response(
            title=f"OPS Installation",
            prompt="OPS Configured. Begin installation?",
            choices=("Begin", ),
            step_name="begin_install"
        )

    await perform_install_steps(install_steps, dependency_graph, text_blocks, configurations['all_steps'], chosen_conf=="manual")

    await asyncio.sleep(1)
    await logger.info("Installation complete", {"step_name": "complete"})

    await get_response(
                title=f"Installation Complete",
                prompt="Press enter to exit",
                choices=("Exit", ),
                step_name="complete"
            )

    exit_()


async def main():

    all_tasks = asyncio.gather(create_app(), install())
    set_all_tasks(all_tasks)

    try:
        await all_tasks
    except asyncio.exceptions.CancelledError:
        # TODO: Catch this exception at other locations and perform cleanup as needed
        pass

    await logger.shutdown()


if __name__ == "__main__":
    asyncio.run(main())