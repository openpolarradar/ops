"""
Prepare postgres for bacula backup and perform cleanup afterwards.

There must exist a /home/backups directory with 0711 permissions owned by postgres.
The postgresql.conf file should include the following settings:

  wal_level = replica
  archive_mode = on
  archive_command = 'test ! -f /home/backups/pg_wal/%f && cp %p /home/backups/pg_wal/%f'
  archive_timeout = 3600
  restore_command = 'cp /home/backups/restore/%f %p'

This script is called before and after a OPS bacula backup. The bacula backup job
should call this script via the following directives:

  RunScript {
    RunsWhen = Before
    FailJobOnError = Yes  # If could not move old backup, do not perform this one
    Command = "/usr/bin/venv/bin/python /opt/ops/conf/tools/postgres_backup.py pre %l"
  }
  RunScript {
    RunsWhen = After
    FailJobOnError = No  # If rename fails, job still successful -- next prescript will fix
    RunsOnFailure	= No  # If job fails, do not delete old WAL files!!
    Command = "/usr/bin/venv/bin/python /opt/ops/conf/tools/postgres_backup.py post %l"
  }

Author: Reece Mathews
Python Version: 3.8.7
"""
from pathlib import Path
from datetime import date
import shutil
import sys
import subprocess
import os
import re
import traceback


DELETE_OLD_BACKUPS = True  # Delete old backups after a successful backup of base dir
MAX_OLD_BACKUPS = 0  # The number of old base-dir backups to keep on disk
POSTGRES_UID = int(subprocess.check_output(["id", "postgres", '-u']).decode().strip())
# BACKUP_BASE_DIR = Path('/home/')
BACKUP_BASE_DIR = Path('/')


def get_label(target_dir):
    """Read the label in backup_label in target_dir"""

    label = None
    try:
        with (target_dir / "backup_label").open() as f:
            for line in f:
                if line.strip().startswith("LABEL"):
                    label = line.strip().split(": ")[1]
                    break
    except FileNotFoundError:
        pass

    return label


def rename_current_backup():
    """Move and rename the backup directory if it exists to the date it was made."""

    target_dir = BACKUP_BASE_DIR / Path("backups/base")
    if not target_dir.exists():
        return False

    label = get_label(target_dir)
    if label is None:
        label = date.today().strftime("%Y-%m-%d_unlabeled")

    destination = BACKUP_BASE_DIR / Path("backups/old") / label
    attempt = 1
    while True:
        if destination.exists():
            destination = Path("backups/old") / (label + "." + str(attempt))
            attempt += 1
        else:
            break

    shutil.move(str(target_dir), destination)
    return label


def is_pre():
    """Determine if run time (first argument) is pre-backup"""
    assert len(sys.argv) > 2

    return sys.argv[1] == "pre"


def is_full():
    """Determine if runlevel (second argument) denotes a Full backup"""
    assert len(sys.argv) > 2

    return sys.argv[2] == "Full"


def set_postgres():
    """Set process owner to postgres user."""
    os.setuid(POSTGRES_UID)


def create_backup():
    # Perform pg_basebackup
    date_label = date.today().strftime("%Y-%m-%d")
    proc = None
    try:
        proc = subprocess.run(['pg_basebackup', '-D', str(BACKUP_BASE_DIR / 'backups/base'), '-l', date_label],
                              capture_output=True, preexec_fn=set_postgres)
        proc.check_returncode()
    except subprocess.CalledProcessError as e:
        if proc is not None and proc.stderr:
            print(proc.stderr, file=sys.stderr)
        if proc is not None and proc.stdout:
            print(proc.stdout)
        traceback.print_exception(e)
        exit(1)

    # Check that directory was created
    target_dir = BACKUP_BASE_DIR / Path("backups/base")
    if not target_dir.exists():
        print(target_dir, "does not exist")
        exit(1)


def sort_backup(backup):
    """Return a number for use in sorting backups by name."""
    backup = backup.name.replace("-", "")
    backup = backup.replace("_unlabeled", "")
    return float(backup)


def find_old_backups():
    """
    Get a list of backups in the old directory in order of creation date and filename
    such that the newest of the old backups comes last and the oldest is first.
    """
    backups = sorted((BACKUP_BASE_DIR / Path("backups/old") / backup for backup
                      in os.listdir(BACKUP_BASE_DIR / "backups/old") if backup != "pg_wal"),
                     key=os.path.getmtime)
    return sorted(backups, key=sort_backup)


def delete_old_backups():
    """Delete all backups in the old directory except MAX_OLD_BACKUPS most recent."""
    if MAX_OLD_BACKUPS == 0:
        list_slice = slice(0, None)
    else:
        list_slice = slice(0, -MAX_OLD_BACKUPS)

    for backup in find_old_backups()[list_slice]:
        shutil.rmtree(str(BACKUP_BASE_DIR / Path("backups/old") / backup))


def get_WAL_start(backup):
    """Get the WAL file start from the given backup."""
    target_file = backup / "backup_label"
    start_WAL = None
    try:
        with target_file.open() as f:
            for line in f:
                matches = re.match(r"START WAL LOCATION: .* \(file (.*)\)", line.strip())
                if matches:
                    start_WAL = matches[1].strip()
                    break
    except FileNotFoundError:
        print("Could not fine a WAL file")
        exit(1)

    return start_WAL


def delete_old_WAL(latest_backup):
    """Delete WAL files older than the start WAL file."""

    # Find start of WAL files from latest backup
    start_WAL = get_WAL_start(latest_backup)
    if start_WAL is None:
        print("Could not find the starting WAL file")
        exit(1)

    # WAL files can be compared sequentially by converting the filename to hexidecimal
    start_WAL_hex = int(start_WAL, base=16)

    wals = os.listdir(BACKUP_BASE_DIR / "backups/pg_wal")
    wals = ((int(x.split(".")[0], base=16), x) for x in wals)

    # Iterate all WAL files in pg_wal and move ones older than start_WAL
    for wal in sorted(wals, key=lambda x: x[0]):
        if wal[0] < start_WAL_hex:
            # Should be safe to delete
            shutil.move(str(BACKUP_BASE_DIR / Path("backups/pg_wal") / wal[1]), str(BACKUP_BASE_DIR / Path("backups/old/pg_wal") / wal[1]))
        else:
            break


if __name__ == "__main__":

    # Only do anything for full backups
    if not is_full():
        exit(0)

    os.makedirs(BACKUP_BASE_DIR / "backups/old/pg_wal", exist_ok=True)
    os.makedirs(BACKUP_BASE_DIR / "backups/pg_wal", exist_ok=True)

    os.chown(BACKUP_BASE_DIR / 'backups', POSTGRES_UID, gid=-1)
    os.chown(BACKUP_BASE_DIR / 'backups/old', POSTGRES_UID, gid=-1)
    os.chown(BACKUP_BASE_DIR / 'backups/old/pg_wal', POSTGRES_UID, gid=-1)
    os.chown(BACKUP_BASE_DIR / 'backups/pg_wal', POSTGRES_UID, gid=-1)

    if is_pre():
        # Move base directory if it exists
        rename_current_backup()

    if DELETE_OLD_BACKUPS:
        # Delete old base dirs, keeping as many as specified by MAX_OLD_BACKUPS
        delete_old_backups()

    if not is_pre():
        # Check that base directory still exists
        target_dir = BACKUP_BASE_DIR / Path("backups/base")
        if not target_dir.exists():
            print(target_dir, "does not exist")
            exit(1)

    # Start backup if prescript
    if is_pre():
        print("Creating backup")
        create_backup()

    # Find latest backup and delete old backups, whether post or pre (for redundancy)
    base_dir = BACKUP_BASE_DIR / Path("backups/base")
    if base_dir.exists():
        latest_backup = base_dir
    else:
        latest_backups = find_old_backups()
        if not latest_backups:
            # There should be one that was just made when this script was ran as pre-backup
            print("latest backup missing")
            exit(1)
        latest_backup = latest_backups[-1]

    # Delete old WAL files that are no longer necessary
    delete_old_WAL(latest_backup)
