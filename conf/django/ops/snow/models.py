from django.contrib.gis.db import models


class locations(models.Model):
    class Meta:
        verbose_name = 'Location'
        verbose_name_plural = 'Locations'

    name = models.CharField(max_length=20)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class seasons(models.Model):
    class Meta:
        verbose_name = 'Season'
        verbose_name_plural = 'Seasons'

    location = models.ForeignKey("locations", on_delete=models.CASCADE)
    season_group = models.ForeignKey("season_groups", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class season_groups(models.Model):
    class Meta:
        verbose_name = 'SeasonGroup'
        verbose_name_plural = 'SeasonGroups'

    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=True)
    public = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class radars(models.Model):
    class Meta:
        verbose_name = 'Radar'
        verbose_name_plural = 'Radars'

    name = models.CharField(max_length=20)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class organization(models.Model):
    class Meta:
        verbose_name = 'Organization'
        verbose_name_plural = 'Organizations'

    name = models.CharField(max_length=255)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class segments(models.Model):
    class Meta:
        verbose_name = 'Segment'
        verbose_name_plural = 'Segments'

    season = models.ForeignKey("seasons", on_delete=models.CASCADE)
    radar = models.ForeignKey("radars", on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    geom = models.LineStringField()
    objects = models.Manager()
    crossover_calc = models.BooleanField(default=True)

    organizations = models.ManyToManyField(organization)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class dois(models.Model):
    class Meta:
        verbose_name = 'Doi'
        verbose_name_plural = 'Dois'
    segment = models.ForeignKey("segments", on_delete=models.CASCADE)
    doi = models.CharField(max_length=255)

    def __str__(self):
        return "%s" % (self.doi)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.doi}', {self.segment}]"


class funding(models.Model):
    class Meta:
        verbose_name = 'Funding'
        verbose_name_plural = 'Fundings'
    segment = models.ForeignKey("segments", on_delete=models.CASCADE)
    source = models.CharField(max_length=255)

    def __str__(self):
        return "%s" % (self.source)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.source}', {self.segment}]"


class frames(models.Model):
    class Meta:
        verbose_name = 'Frame'
        verbose_name_plural = 'Frames'

    segment = models.ForeignKey("segments", on_delete=models.CASCADE)
    name = models.CharField(max_length=20)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class point_paths(models.Model):
    class Meta:
        verbose_name = 'PointPath'
        verbose_name_plural = 'PointPaths'

    location = models.ForeignKey("locations", on_delete=models.CASCADE)
    season = models.ForeignKey("seasons", on_delete=models.CASCADE)
    segment = models.ForeignKey("segments", on_delete=models.CASCADE)
    frame = models.ForeignKey("frames", on_delete=models.CASCADE)
    gps_time = models.DecimalField(max_digits=16, decimal_places=6, db_index=True)
    roll = models.DecimalField(max_digits=6, decimal_places=5)
    pitch = models.DecimalField(max_digits=6, decimal_places=5)
    heading = models.DecimalField(max_digits=6, decimal_places=5)
    geom = models.PointField(dim=3)
    objects = models.Manager()
    key_point = models.BooleanField(default=True, db_index=True)

    def __str__(self):
        return "%d" % (self.id)

    def __repr__(self):
        return f"{self._meta.verbose_name}[{self.segment}, {self.gps_time}]"


class crossovers(models.Model):
    class Meta:
        verbose_name = 'Crossover'
        verbose_name_plural = 'Crossovers'

    point_path_1 = models.ForeignKey(
        "point_paths", related_name="point_paths_link_fk_1", on_delete=models.CASCADE
    )
    point_path_2 = models.ForeignKey(
        "point_paths", related_name="point_paths_link_fk_2", on_delete=models.CASCADE
    )
    angle = models.DecimalField(max_digits=6, decimal_places=3)
    geom = models.PointField()
    objects = models.Manager()

    def __str__(self):
        return "%d" % (self.id)

    def __repr__(self):
        return f"{self._meta.verbose_name}[{self.point_path_1}, {self.point_path_2}]"


class layer_groups(models.Model):
    class Meta:
        verbose_name = 'LayerGroup'
        verbose_name_plural = 'LayerGroups'

    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=True)
    public = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class layers(models.Model):
    class Meta:
        verbose_name = 'Layer'
        verbose_name_plural = 'Layers'

    id = models.AutoField(primary_key=True)
    layer_group = models.ForeignKey("layer_groups", on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=200, blank=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return f"{self._meta.verbose_name}['{self.name}']"


class layer_links(models.Model):
    class Meta:
        verbose_name = 'LayerLink'
        verbose_name_plural = 'LayerLinks'

    layer_1 = models.ForeignKey("layers", related_name="layer_link_fk_1", on_delete=models.CASCADE)
    layer_2 = models.ForeignKey("layers", related_name="layer_link_fk_2", on_delete=models.CASCADE)

    def __str__(self):
        return "%d" % (self.id)

    def __repr__(self):
        return f"{self._meta.verbose_name}[{self.layer_1}, {self.layer_2}]"


class layer_points(models.Model):
    # import User for 'user_id' column
    from django.contrib.auth.models import User
    class Meta:
        unique_together = ("layer", "point_path")
        verbose_name = 'LayerPoint'
        verbose_name_plural = 'LayerPoints'

    layer = models.ForeignKey("layers", on_delete=models.CASCADE)
    point_path = models.ForeignKey("point_paths", on_delete=models.CASCADE)
    twtt = models.DecimalField(max_digits=12, decimal_places=11, blank=True, null=True)
    type = models.IntegerField(blank=True, null=True)  # 1:manual,2:auto
    quality = models.IntegerField(blank=True, null=True)  # 1:good,2:moderate,3:derived
    user = models.ForeignKey(User, related_name="snow_user_id_fk", on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%d" % (self.id)

    def __repr__(self):
        return f"{self._meta.verbose_name}[{self.layer}, {self.point_path}]"


class landmarks(models.Model):
    class Meta:
        verbose_name = 'Landmark'
        verbose_name_plural = 'Landmarks'

    radar = models.ForeignKey("radars", on_delete=models.CASCADE)
    segment = models.ForeignKey("segments", on_delete=models.CASCADE)
    point_path_1 = models.ForeignKey(
        "point_paths", related_name="point_paths_link_fk2_1", on_delete=models.CASCADE
    )
    point_path_2 = models.ForeignKey(
        "point_paths", related_name="point_paths_link_fk2_2", on_delete=models.CASCADE
    )
    start_twtt = models.DecimalField(max_digits=12, decimal_places=11, db_index=True)
    stop_twtt = models.DecimalField(max_digits=12, decimal_places=11, db_index=True)
    description = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return "%d" % (self.id)

    def __repr__(self):
        return f"{self._meta.verbose_name}[{self.point_path_1}, {self.point_path_2}]"