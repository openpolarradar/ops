from django.contrib import admin
from opsuser.models import *


class opsAdmin(admin.ModelAdmin):

    # Make models use their __repr__ field to display themselves in the admin site
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        field = super().formfield_for_foreignkey(db_field, request, **kwargs)
        if field:
            field.label_from_instance = repr
        return field

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        field = super().formfield_for_manytomany(db_field, request, **kwargs)
        if field:
            field.label_from_instance = repr
        return field


@admin.register(UserProfile)
class UserProfileAdmin(opsAdmin):
    list_display = ['user', 'layerGroupRelease', 'seasonRelease', 'createData', 'bulkDeleteData', 'isRoot', 'rds_season_groups_', 'rds_layer_groups_', 'accum_season_groups_', 'accum_layer_groups_', 'snow_season_groups_', 'snow_layer_groups_', 'kuband_season_groups_', 'kuband_layer_groups_']

    @admin.display(ordering="rds_season_groups__name")
    def rds_season_groups_(self, obj):
        return obj.rds_season_groups.name
    @admin.display(ordering="rds_layer_groups__name")
    def rds_layer_groups_(self, obj):
        return obj.rds_layer_groups.name
    @admin.display(ordering="accum_season_groups__name")
    def accum_season_groups_(self, obj):
        return obj.accum_season_groups.name
    @admin.display(ordering="accum_layer_groups__name")
    def accum_layer_groups_(self, obj):
        return obj.accum_layer_groups.name
    @admin.display(ordering="snow_season_groups__name")
    def snow_season_groups_(self, obj):
        return obj.snow_season_groups.name
    @admin.display(ordering="snow_layer_groups__name")
    def snow_layer_groups_(self, obj):
        return obj.snow_layer_groups.name
    @admin.display(ordering="kuband_season_groups__name")
    def kuband_season_groups_(self, obj):
        return obj.kuband_season_groups.name
    @admin.display(ordering="kuband_layer_groups__name")
    def kuband_layer_groups_(self, obj):
        return obj.kuband_layer_groups.name