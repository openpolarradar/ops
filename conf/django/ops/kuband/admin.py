from django.contrib import admin
from kuband.models import *


class opsAdmin(admin.ModelAdmin):

    # Make models use their __repr__ field to display themselves in the admin site
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        field = super().formfield_for_foreignkey(db_field, request, **kwargs)
        if field:
            field.label_from_instance = repr
        return field

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        field = super().formfield_for_manytomany(db_field, request, **kwargs)
        if field:
            field.label_from_instance = repr
        return field


@admin.register(locations)
class locationsAdmin(opsAdmin):
    list_display = ['id', 'name']


@admin.register(seasons)
class seasonsAdmin(opsAdmin):
    list_display = ['name', 'location', 'season_group']


@admin.register(season_groups)
class season_groupsAdmin(opsAdmin):
    list_display = ['name', 'description', 'public']


@admin.register(radars)
class radarsAdmin(opsAdmin):
    list_display = ['name']


@admin.register(organization)
class organizationAdmin(opsAdmin):
    list_display = ['name']


@admin.register(segments)
class segmentsAdmin(opsAdmin):
    list_display = ['season', 'radar', 'name', 'crossover_calc', 'organizations_']

    @admin.display(ordering="organizations__name")
    def organizations_(self, obj):
        return obj.organizations.name


@admin.register(dois)
class doisAdmin(opsAdmin):
    list_display = ['segment', 'doi']


@admin.register(funding)
class fundingAdmin(opsAdmin):
    list_display = ['segment', 'source']


@admin.register(frames)
class framesAdmin(opsAdmin):
    list_display = ['segment', 'name']


@admin.register(point_paths)
class point_pathsAdmin(opsAdmin):
    list_display = ['location', 'season', 'segment', 'frame', 'gps_time', 'roll', 'pitch', 'heading', 'key_point']


@admin.register(crossovers)
class crossoversAdmin(opsAdmin):
    list_display = ['point_path_1', 'point_path_2', 'angle', 'geom']


@admin.register(layer_groups)
class layer_groupsAdmin(opsAdmin):
    list_display = ['name', 'description', 'public']


@admin.register(layers)
class layersAdmin(opsAdmin):
    list_display = ['layer_group', 'name', 'description', 'deleted']


@admin.register(layer_links)
class layer_linksAdmin(opsAdmin):
    list_display = ['layer_1', 'layer_2']


@admin.register(layer_points)
class layer_pointsAdmin(opsAdmin):
    list_display = ['layer', 'point_path', 'twtt', 'type', 'quality', 'user', 'last_updated']


@admin.register(landmarks)
class landmarksAdmin(opsAdmin):
    list_display = ['radar', 'segment', 'point_path_1', 'point_path_2', 'start_twtt', 'stop_twtt', 'description']


