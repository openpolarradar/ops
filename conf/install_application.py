import re
import asyncio
import xml

from typing import Dict, Any, List, Sequence, Tuple, Optional
from collections import namedtuple, defaultdict

from prompt_toolkit.shortcuts import checkboxlist_dialog, button_dialog, radiolist_dialog, ProgressBar
from prompt_toolkit.formatted_text import to_formatted_text, HTML, AnyFormattedText

from prompt_toolkit.data_structures import Point
from prompt_toolkit import Application
from prompt_toolkit.key_binding import KeyBindings, merge_key_bindings
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.layout.containers import VSplit, Window, HSplit
from prompt_toolkit.layout.controls import BufferControl, FormattedTextControl
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.layout import DynamicContainer, Container, ConditionalContainer
from prompt_toolkit.application import get_app
from prompt_toolkit.widgets import RadioList, TextArea, Dialog, Label, Button, CheckboxList
from prompt_toolkit.styles import BaseStyle
from prompt_toolkit.filters import Filter
from prompt_toolkit.key_binding.bindings.focus import focus_next, focus_previous
from prompt_toolkit.key_binding.defaults import load_key_bindings
from prompt_toolkit.layout.dimension import Dimension
from prompt_toolkit.styles import Style


kb = KeyBindings()
kb.add("tab")(focus_next)
kb.add("s-tab")(focus_previous)

input_container = TextArea()
def get_input_container():
    return input_container

progress_window = FormattedTextControl(None)

# TODO: There has to be a way to get the current height dynamically
OUTPUT_HEIGHT = 15

show_prompt = False
prompt_event = asyncio.Event()
all_tasks = None

class PromptWindowFilter(Filter):
    def __call__(self):
        return show_prompt


dynamic_container = ConditionalContainer(DynamicContainer(get_input_container),
                                         PromptWindowFilter())
output = FormattedTextControl(None, focusable=False, show_cursor=False)
output_window = Window(output, height=OUTPUT_HEIGHT)
submit_callback = lambda: None
root_container = None

log_colors = {
    "[INFO ]": "skyblue",
    "[WARNI]": "lightsalmon",
    "[ERROR]": "red",
    "[DEBUG]": "springgreen"
}
task_colors = {
    "[IN PROGRESS]": "skyblue",
    "[COMPLETE   ]": "springgreen",
    "[ERROR      ]": "red",
    "[SKIPPED    ]": "lightgray",
    "[CANCELLED  ]": "darkgray",
    "[TODO       ]": "lightsalmon",
    "[WAITING    ]": "orchid",
    "[RESOLVING  ]": "DarkSlateBlue"
}

LAYER_EMPTY = "."
LAYER_SOURCE = "╻"
LAYER_PASS = "┊"
LAYER_MARK = "┃"
LAYER_LAST = "╹"


def format_log(text: str):
    """Format log output with HTML."""
    matches = re.match(r'(.*)(\[((DEBUG)|(WARNI)|(ERROR)|(INFO ))\]) (\(.*?\)) (.*)', text)
    if matches:
        prefix = matches[1]
        level = matches[2]
        step = matches[8]
        msg = matches[9]

        color = log_colors.get(level, "white")

        text = f"<gray>{prefix}</gray><{color}>{level}</{color}> <gray>{step}</gray>"

        return to_formatted_text(HTML(text)) + to_formatted_text(msg) + to_formatted_text(HTML("\n"))

    return to_formatted_text(text) + to_formatted_text(HTML("\n"))


def get_formatted_lines(text):
    """Group formatted text into lines based on location of newlines."""
    lines = [[]]
    for text_obj in text:
        if "\n" in text_obj[1]:
            lines.append([])
        lines[-1].append(text_obj)

    return lines


def flatten_lines(lines: list[tuple[str, str]]):
    """Return lists of lines into a flat list of text objects."""
    text = []
    for line in lines:
        for text_ in line:
            text.append(text_)

    return text

def write_output(text: str):
    """Write to the output window."""
    if output.text is None:
        output.text = format_log(text)
    else:
        output.text += format_log(text)
        lines = get_formatted_lines(output.text)
        lines = lines[-OUTPUT_HEIGHT:]
        output.text = flatten_lines(lines)

    get_app().invalidate()


def get_free_index(layer: List[Tuple[str, str]]):
    """Get the leftmost open slot in a layer of the dependency graph."""
    for index, slot in enumerate(layer):
        if slot[1] == LAYER_EMPTY:
            return index

    return len(layer)


def crop_layers(layers: List[List[Tuple[str, str]]]):
    """
    Strip empty slots from the right side of the layers to the widest layer.
    Operates in-place and returns the list as well.
    """

    last_slot = 0
    for layer in layers:
        for i, slot in enumerate(layer):
            if slot[1] != LAYER_EMPTY:
                last_slot = max(last_slot, i)

    for layer_num, layer in enumerate(layers):
        layers[layer_num] = layer[:last_slot + 1]

    return layers


def construct_dependency_graph(step_order: List[str], dependency_graph: Dict[str, set[str]]):
    """Construct a ascii graph layer by layer which denotes step dependencies"""

    dependents_graph = {
        step: set(dependent for dependent, dependencies in dependency_graph.items() if step in dependencies)
        for step in step_order
    }

    layers: List[List[Tuple[str, str]]] = [[("", LAYER_EMPTY)] * len(step_order) for _ in step_order]
    # Iterate each step
    for step_num, step in enumerate(step_order):
        if not dependents_graph[step]:
            continue

        slot = get_free_index(layers[step_num])
        layers[step_num][slot] = (step, LAYER_SOURCE)

        # Iterate all subsequent steps and find dependents
        found_dependents = dependents_graph[step].copy()
        for step_dep_num, step_dep in enumerate(step_order[step_num + 1:]):
            if not found_dependents:
                # Found all dependents. This line has completed
                break

            # If this step is has the parent step as a dependency, mark it.
            layer_num = step_num + step_dep_num + 1
            layers[layer_num][slot] = (step, LAYER_PASS)
            if step_dep in found_dependents:
                found_dependents.remove(step_dep)
                if not found_dependents:
                    layers[layer_num][slot] = (step, LAYER_LAST)
                else:
                    layers[layer_num][slot] = (step, LAYER_MARK)

    return crop_layers(layers)


def format_layer(layer: List[Tuple[str, str]], states: Dict[str, str]):
    """Format a layer of the dependency graph."""
    layer_str = ""
    for slot in layer:
        if not slot[0]:
            layer_str += slot[1]
        else:
            state = states[slot[0]]
            color = task_colors[state]
            layer_str += f"<{color}>{slot[1]}</{color}>"

    return to_formatted_text(HTML(layer_str))


def format_step(step: str, state: str, progress: Optional[int], description: str):
    """Format a step for display in the progress window."""
    if progress is None:
        prog_str = ""
    else:
        prog_str = f" {progress}%"
    output = f" {state}{prog_str} {step}{bool(description)*": "}{description}\n"

    return to_formatted_text(HTML(output))


class Progress:

    def __init__(self, step_order: List[str], step_descriptions: Dict[str, str], dependency_graph: Dict[str, set[str]]):
        self.step_order = step_order
        self.step_descriptions = step_descriptions
        self.dependency_graph = dependency_graph
        self.states = defaultdict(lambda: "[TODO       ]")
        self.step_progress = defaultdict(lambda: None)
        self.step_progress_bars = {}

    def set_step_progress_sync(self, step: str, progress: Optional[int]):
        """Sets the progress of a step and updates the progress bar."""
        self.step_progress[step] = progress
        self.update_progress_window()

    async def set_step_progress(self, step: str, progress: Optional[int]):
        """Sets the progress of a step and updates the progress bar."""
        self.step_progress[step] = progress
        self.update_progress_window()
        await asyncio.sleep(0)

    async def set_step_state(self, step: str, state: str, logger=None):
        """Set the state of a step for display in the progress window."""
        if logger:
            await logger.info(f"Step '{step}' marked '{state}'", {"step_name": step})

        self.states[step] = state
        if "IN PROGRESS" not in state:
            self.step_progress[step] = None
        self.update_progress_window()
        await asyncio.sleep(0)

    def update_progress_window(self):
        """Update the progress window to show the current steps."""

        progress_window.text = to_formatted_text(HTML(""))

        layers = construct_dependency_graph(self.step_order, self.dependency_graph)
        for step, layer in zip(self.step_order, layers):

            state = self.states[step]
            description = self.step_descriptions[step]

            color = task_colors[state]
            state = f"<{color}>{state}</{color}>"

            progress_window.text += format_layer(layer, self.states) + \
                                    format_step(step, state, self.step_progress[step],
                                                description)


def set_input_widget(input_widget_: Layout):
    """Set the input widget in the prompt_toolkit application."""
    global input_container, show_prompt

    show_prompt = True
    prompt_event.clear()
    input_container = input_widget_
    try:
        get_app().layout.focus(input_container)
    except ValueError:
        pass  # Cannot focus formattedtextcontrols
    get_app().invalidate()


def produce_submit_callback(return_callback):
    """
    Make a callback that sets the user response variable and alerts the waiting
    coroutine that a response is ready.
    """
    global submit_callback

    def callback(enter=False):
        global prompt_response, show_prompt

        set_input_widget(Window(progress_window))

        try:
            # Try calling callback with whether or not enter was pressed
            prompt_event.response = return_callback(enter)
        except TypeError:
            # Unexpected argument, do not pass enter
            prompt_event.response = return_callback()

        prompt_event.set()

        return prompt_event.response

    submit_callback = callback

    return callback


def create_buttons(
        title: AnyFormattedText = "",
        text: AnyFormattedText = "",
        buttons: List[str] = [],
    ):
    """
    Produce a button dialog.
    Mostly copied from prompt-toolkit/src/prompt_toolkit/shortcuts/dialogs.py
    """

    def ok_handler(button_value: str):

        def callback(enter=False):
            if enter:
                # Find the currently focused button and return the text on it
                get_app().layout.focus_last()  # move focus back to the button that was just selected
                focused_button = get_app().layout.current_control
                for text_item in focused_button.text():
                    text = text_item[1]
                    if text not in ["<", ">", ""]:
                        return text.strip()

                raise RuntimeError("Button doesn't appear to be focused")
            else:
                # Button was clicked. Just return the button value
                if button_value == "Cancel":
                    return None
                return button_value

        return callback


    dialog = Dialog(
        title=title,
        body=Label(text=text, dont_extend_height=True),
        buttons=[
            Button(text=option, handler=produce_submit_callback(ok_handler(option)))
            for option in list(buttons) + ["Cancel"]
        ],
        with_background=True,
    )

    return dialog


def create_radiolist(
        title: AnyFormattedText = "",
        text: AnyFormattedText = "",
        ok_text: str = "Ok",
        cancel_text: str = "Cancel",
        values: List[tuple[Any, AnyFormattedText]] | None = None,
        default: Any | None = None,
    ):
    """
    Produce a radiolist dialog.
    Mostly copied from prompt-toolkit/src/prompt_toolkit/shortcuts/dialogs.py
    """
    if values is None:
        values = []

    radio_list = RadioList(values=values, default=default)
    radio_list.show_scrollbar = False

    dialog = Dialog(
        title=title,
        body=HSplit(
            [Label(text=text, dont_extend_height=True), radio_list],
            padding=1,
        ),
        buttons=[
            Button(text=ok_text, handler=produce_submit_callback(lambda: radio_list.current_value)),
            Button(text=cancel_text, handler=exit_),
        ],
    )

    return dialog


def create_checkboxlist(
        title: AnyFormattedText = "",
        text: AnyFormattedText = "",
        ok_text: str = "Ok",
        cancel_text: str = "Cancel",
        values: List[tuple[Any, AnyFormattedText]] | None = None,
        default_values: List[Any] | None = None,
        style: BaseStyle | None = None,
    ):
    """
    Produce a checkbox list dialog.
    Mostly copied from prompt-toolkit/src/prompt_toolkit/shortcuts/dialogs.py
    """
    if values is None:
        values = []

    cb_list = CheckboxList(values=values, default_values=default_values)
    cb_list.show_scrollbar = False

    dialog = Dialog(
        title=title,
        body=HSplit(
            [Label(text=text, dont_extend_height=True), cb_list],
            padding=1,
        ),
        buttons=[
            Button(text=ok_text, handler=produce_submit_callback(lambda: cb_list.current_values)),
            Button(text=cancel_text, handler=exit_),
        ],
        with_background=True,
    )

    return dialog


def create_input(
        title: AnyFormattedText = "",
        text: AnyFormattedText = "",
        ok_text: str = "OK",
        cancel_text: str = "Cancel",
        password: bool = False,
        default: str = "",
    ):
    """
    Produce a free response dialog.
    Mostly copied from prompt-toolkit/src/prompt_toolkit/shortcuts/dialogs.py
    """

    ok_button = Button(text=ok_text, handler=produce_submit_callback(lambda: textfield.text.strip()))
    cancel_button = Button(text=cancel_text, handler=exit_)

    textfield = TextArea(
        text=default,
        multiline=False,
        password=password,
    )

    dialog = Dialog(
        title=title,
        body=HSplit(
            [
                Label(text=text, dont_extend_height=True),
                textfield
            ],
            padding=Dimension(preferred=1, max=1),
        ),
        buttons=[ok_button, cancel_button],
        with_background=True,
    )

    return dialog

def format_option_desc(option: Dict[str, Any]):
    """Format the description of an option from the yaml as prompt_toolkit HTML"""
    name = "<b>" + option['name'].replace("_", " ").title() + "</b>"
    desc = name + ": " + option.get('description', '')
    desc_formatted = to_formatted_text(HTML(desc.removesuffix(': ')))

    return desc_formatted


def format_options(options: List[Dict[str, Any]]):
    """Format the options provided in a configuration from the yaml for prompt_toolkit."""
    formatted_options = []
    for option in options:
        # Place name in description and format as bold
        formatted_options.append((option['name'], format_option_desc(option)))

    return formatted_options


@kb.add('escape')
def exit_(_=None):
    # TODO[reece]: Handle aborts better and interrupts
    get_app().exit()

    if all_tasks:
        all_tasks.cancel()


@kb.add('enter', is_global=True)
def submit_(_=None):
    submit_callback(enter=True)


def set_all_tasks(all_tasks_):
    global all_tasks

    all_tasks = all_tasks_


async def create_app():
    """Create the prompt_tookit application used to display configuration options."""
    global root_container

    root_container = HSplit([
        output_window,
        dynamic_container,
    ], padding=1)

    layout = Layout(root_container, focused_element=input_container)

    bindings = merge_key_bindings([load_key_bindings(), kb])
    app = Application(key_bindings=bindings, layout=layout, full_screen=True,
                      mouse_support=True, style=Style.from_dict({
                        'radio-selected': 'bg:#57b5ff #fff',
                        'radio-checked': '#57b5ff',
                        'dialog.body': 'bg:#282b34 #fff',
                        'frame.label': '#57b5ff',
                        'button.focused': 'bg: #fff',
                        "dialog": "bg:#282b34",
                        "text-area": "bg: #5f626c #fff",
                        "text-area.prompt": "bg: #5f626c #fff",
                    }), refresh_interval=1)
    await app.run_async()  # TODO: Use sigint callback??
