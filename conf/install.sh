#!/bin/bash
# Install a few tools and run the python provision.py script
# Python install script from https://www.build-python-from-source.com/

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# optimizations=0;
# while true; do
#     read -p "Would you like to compile python with optimizations (takes longer to compile but makes python faster)? [y/n]" yn
#     case $yn in
#         [Yy]* )
#             optimizations=1;
#             break;;

#         [Nn]* ) break;;
#         * ) echo "Please answer yes or no.";;
#     esac
# done

dnf -y update
# dnf -y groupinstall "Development Tools"
# dnf -y install wget gcc openssl-devel bzip2-devel libffi-devel xz-devel tk-devel readline-devel

# cd /tmp/
# wget https://www.python.org/ftp/python/3.12.0/Python-3.12.0.tgz
# tar xzf Python-3.12.0.tgz
# cd Python-3.12.0

# # LD_FLAGS from here: https://stackoverflow.com/a/56088070/7587147
# if [ "$optimizations" -eq 1 ]; then
#     ./configure --prefix=/opt/python/3.12.0/ --with-lto --with-computed-gotos --enable-optimizations --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
# else
#     ./configure --prefix=/opt/python/3.12.0/ --with-lto --with-computed-gotos --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
# fi

# make -j "$(nproc)"
# make altinstall
# rm /tmp/Python-3.12.0.tgz

# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/python/3.12.0/lib/
# echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/opt/python/3.12.0/lib/" >> ~/.bashrc
# # Add .so link to lib64. Seems to be more reliable than adding path to bashrc for vscode
# ln -s /opt/python/3.12.0/lib/libpython3.12.so.1.0 /lib64/libpython3.12.so.1.0

# /opt/python/3.12.0/bin/python3.12 -m pip install --upgrade pip setuptools wheel

# ln -sf /opt/python/3.12.0/bin/python3.12        /opt/python/3.12.0/bin/python3
# ln -sf /opt/python/3.12.0/bin/python3.12        /opt/python/3.12.0/bin/python
# ln -sf /opt/python/3.12.0/bin/pip3.12           /opt/python/3.12.0/bin/pip3
# ln -sf /opt/python/3.12.0/bin/pip3.12           /opt/python/3.12.0/bin/pip
# ln -sf /opt/python/3.12.0/bin/pydoc3.12         /opt/python/3.12.0/bin/pydoc
# ln -sf /opt/python/3.12.0/bin/idle3.12          /opt/python/3.12.0/bin/idle
# ln -sf /opt/python/3.12.0/bin/python3.12-config      /opt/python/3.12.0/bin/python-config

dnf install -y python3.12 python3.12-devel

cd /opt/ops

python3.12 -m venv /usr/bin/venv
source /usr/bin/venv/bin/activate

echo 'source /usr/bin/venv/bin/activate' >> ~/.bashrc

python -m pip install --upgrade pip
pip install -r conf/requirements.txt

python conf/provision.py
