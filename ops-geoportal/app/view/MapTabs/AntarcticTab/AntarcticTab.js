var antarcticWms = OPS.Global.baseUrl.concat('/geoserver/antarctic/wms');
var selected_segment = null;
var selected_season = null;

/* ---- FIX preventdefault PROBLEMS WITH OLD OPENLAYERS IN CHROME */
// from https://stackoverflow.com/a/55960462
const eventListenerOptionsSupported = () => {
	let supported = false;

	try {
	  const opts = Object.defineProperty({}, 'passive', {
		get() {
		  supported = true;
		}
	  });

	  window.addEventListener('test', null, opts);
	  window.removeEventListener('test', null, opts);
	} catch (e) {}

	return supported;
}

const defaultOptions = {
	passive: false,
	capture: false
};
const supportedPassiveTypes = [
	'scroll', 'wheel',
	'touchstart', 'touchmove', 'touchenter', 'touchend', 'touchleave',
	'mouseout', 'mouseleave', 'mouseup', 'mousedown', 'mousemove', 'mouseenter', 'mousewheel', 'mouseover'
];
const getDefaultPassiveOption = (passive, eventName) => {
	if (passive !== undefined) return passive;

	return supportedPassiveTypes.indexOf(eventName) === -1 ? false : defaultOptions.passive;
};

const getWritableOptions = (options) => {
	const passiveDescriptor = Object.getOwnPropertyDescriptor(options, 'passive');

	return passiveDescriptor && passiveDescriptor.writable !== true && passiveDescriptor.set === undefined
	  ? Object.assign({}, options)
	  : options;
};

const overwriteAddEvent = (superMethod) => {
	EventTarget.prototype.addEventListener = function (type, listener, options) {
	  const usesListenerOptions = typeof options === 'object' && options !== null;
	  const useCapture          = usesListenerOptions ? options.capture : options;

	  options         = usesListenerOptions ? getWritableOptions(options) : {};
	  options.passive = getDefaultPassiveOption(options.passive, type);
	  options.capture = useCapture === undefined ? defaultOptions.capture : useCapture;

	  superMethod.call(this, type, listener, options);
	};

	EventTarget.prototype.addEventListener._original = superMethod;
};

const supportsPassive = eventListenerOptionsSupported();

if (supportsPassive) {
const addEvent = EventTarget.prototype.addEventListener;
overwriteAddEvent(addEvent);
}
/* ---- */

var antarcticMapPanel = Ext.create('GeoExt.panel.Map', {
	region: 'center',
	itemId: 'antarcticMap',
	map: {
		allOverlays: false,
		projection: 'EPSG:3031',
		displayProjection: new OpenLayers.Projection("EPSG:4326"),
		units: 'dd',
		maxExtent: new OpenLayers.Bounds(-8221026, -4585257, 8091116, 4715069),
		controls: [
			new OpenLayers.Control.Navigation({ dragPanOptions: { enableKinetic: true } }),
			new OpenLayers.Control.Zoom(),
			new OpenLayers.Control.MousePosition({ prefix: '<a target="_blank" href="https://spatialreference.org/ref/epsg/3031/">WGS:84</a>: ', separator: ' E; ', suffix: ' N' }),
			new OpenLayers.Control.ScaleLine({ geodesic: true })
		]
	},
	center: [397009, 0],
	zoom: 3,
	layers: [
		new OpenLayers.Layer.WMS("Natural Earth I", antarcticWms, { layers: 'antarctic:antarctic_naturalearth' }, { isBaseLayer: true }),
		new OpenLayers.Layer.WMS("Lansat7 (LIMA) 240m", antarcticWms, { layers: 'antarctic:antarctica_lima_240m' }, { isBaseLayer: true }),
		new OpenLayers.Layer.WMS("Bedmap2 Bed", antarcticWms, { layers: 'antarctic:antarctica_bedmap2_bed' }, { isBaseLayer: true }),
		new OpenLayers.Layer.WMS("Measures Velocity", antarcticWms, { layers: 'antarctic:antarctica_measures_velocity_log_magnitude' }, { isBaseLayer: true }),
		new OpenLayers.Layer.WMS("Antarctica Coastline", antarcticWms, { layers: 'antarctic:antarctica_coastline', transparent: true }, { isBaseLayer: true, visibility: false }),

		new OpenLayers.Layer.WMS("Radar Depth Sounder", antarcticWms, { layers: 'antarctic:antarctic_rds_line_paths', transparent: true }, { isBaseLayer: false, visibility: true }),
		//new OpenLayers.Layer.WMS("Radar Depth Sounder Crossovers",antarcticWms,{layers: 'antarctic:antarctic_rds_crossover_errors',transparent:true},{isBaseLayer:false,visibility:false}),

		new OpenLayers.Layer.WMS("Accumulation Radar", antarcticWms, { layers: 'antarctic:antarctic_accum_line_paths', transparent: true }, { isBaseLayer: false, visibility: false }),
		//new OpenLayers.Layer.WMS("Accumulation Radar Crossovers",antarcticWms,{layers: 'antarctic:antarctic_accum_crossover_errors',transparent:true},{isBaseLayer:false,visibility:false}),

		new OpenLayers.Layer.WMS("Snow Radar", antarcticWms, { layers: 'antarctic:antarctic_snow_line_paths', transparent: true }, { isBaseLayer: false, visibility: false }),
		//new OpenLayers.Layer.WMS("Snow Radar Crossovers",antarcticWms,{layers: 'antarctic:antarctic_snow_crossover_errors',transparent:true},{isBaseLayer:false,visibility:false}),

		new OpenLayers.Layer.WMS("KuBand Radar", antarcticWms, { layers: 'antarctic:antarctic_kuband_line_paths', transparent: true }, { isBaseLayer: false, visibility: false }),
		//new OpenLayers.Layer.WMS("KuBand Radar Crossovers",antarcticWms,{layers: 'antarctic:antarctic_kuband_crossover_errors',transparent:true},{isBaseLayer:false,visibility:false}),

		antarcticSelectedLine = new OpenLayers.Layer.Vector('', { displayInLayerSwitcher: false })
	]
});

var toolbarItems = [];
mapHelp = Ext.create('Ext.button.Button', {
	text: 'map help',
	handler: function () {
		Ext.Ajax.request({
			url: '/resources/maphelp.html',
			success: function (response) {
				Ext.create('Ext.window.Window', {
					title: 'Map Help Documentation',
					height: 600,
					width: 600,
					layout: 'fit',
					autoScroll: true,
					closable: true,
					padding: '10 10 10 10',
					items: [{
						xtype: 'label',
						html: response.responseText
					}]
				}).show()
			}
		});
	}
});
toolbarItems.push(mapHelp);
toolbarItems.push("-");
action = Ext.create('GeoExt.Action', {
	control: new OpenLayers.Control.ZoomToMaxExtent(),
	map: antarcticMapPanel.map,
	text: "max extent",
	tooltip: "zoom to the maps max extent"
});
toolbarItems.push(Ext.create('Ext.button.Button', action));
toolbarItems.push("-");
var antarcticLineMeasure = new OpenLayers.Control.DynamicMeasure(OpenLayers.Handler.Path, { geodesic: true, maxSegments: 10, immediate: true });
var antarcticAreaMeasure = new OpenLayers.Control.DynamicMeasure(OpenLayers.Handler.Polygon, { geodesic: true, maxSegments: 10, immediate: true });
action = Ext.create('GeoExt.Action', {
	itemId: 'antarcticLineMeasure',
	control: antarcticLineMeasure,
	map: antarcticMapPanel.map,
	text: "measure distance",
	tooltip: "draw a line to measure a distance, click the button again to clear the drawing.",
	enableToggle: true,
	toggleGroup: 'measureTools'
});
toolbarItems.push(Ext.create('Ext.button.Button', action));
action = Ext.create('GeoExt.Action', {
	itemId: 'antarcticAreaMeasure',
	control: antarcticAreaMeasure,
	map: antarcticMapPanel.map,
	text: "measure area",
	tooltip: "draw a polygon to measure an area, click the button again to clear the drawing.",
	enableToggle: true,
	toggleGroup: 'measureTools'
});
toolbarItems.push(Ext.create('Ext.button.Button', action));
toolbarItems.push("-");

antarcticMapPanel.map.events.register(
	"click",
	antarcticMapPanel.map,
	function (clickPt) {

		var clickCoords = antarcticMapPanel.map.getLonLatFromPixel(clickPt.xy);

		var selectedSystem = Ext.ComponentQuery.query('#selectedSystem')[0].value;
		var selectedSeasons = Ext.ComponentQuery.query('#selectedSeasons')[0].value;
		var startDate = Ext.ComponentQuery.query('#startDate')[0].getRawValue();
		var stopDate = Ext.ComponentQuery.query('#stopDate')[0].getRawValue();

		if (!selectedSystem) { alert('ERROR: SELECT SYSTEM IN THE MENU BEFORE CLICKING ON THE MAP.'); return }
		if (!selectedSeasons || selectedSeasons.length == 0) { useSeasons = false; } else { useSeasons = true; }
		if (!startDate) { startDateSeg = '00000000_00'; } else { startDateSeg = startDate.concat('_00'); }
		if (!stopDate) { stopDateSeg = '99999999_99'; } else { stopDateSeg = stopDate.concat('_99'); }

		if (useSeasons) {
			inputJSON = JSON.stringify({ "properties": { "location": "antarctic", "x": clickCoords.lon, "y": clickCoords.lat, "season": selectedSeasons, "startseg": startDateSeg, "stopseg": stopDateSeg } });
		} else {
			inputJSON = JSON.stringify({ "properties": { "location": "antarctic", "x": clickCoords.lon, "y": clickCoords.lat, "startseg": startDateSeg, "stopseg": stopDateSeg } });
		}
		Ext.getBody().mask("Loading Echogram Browser");
		Ext.Ajax.request({
			type: "POST",
			url: '/ops/get/frame/closest',
			params: { app: selectedSystem, data: inputJSON },
			success: function (response) { antarcticRenderClosestFrame(response.responseText) },
			error: function () { alert('ERROR FINDING CLOSEST FRAME ON CLICK EVENT.'); }
		});
	}
);

var selectedDisplay = null;
var copylastSelectedEchogram = null;
function antarcticRenderClosestFrame(response) {

	// decode the response and error check
	responseData = JSON.parse(response);
	if (responseData.status == 0) { alert(responseData.data); return };

	// add the echograms to the dropdown menu
	var outEchogramUrls = [];
	var outEchogramNames = ['Echogram w/o layers', 'Echogram w/ layers', 'Echogram full resolution'];;

	for (var i = 0; i < responseData.data.echograms.length; i++) {
		outEchogramUrls.push([responseData.data.echograms[i], outEchogramNames[i]]);
	};

	var echogramCombo = Ext.ComponentQuery.query('#antarcticEchogramSelector')[0];

	echogramStore = new Ext.data.ArrayStore({
		fields: ['echogram_url', 'echogram_name'],
		data: outEchogramUrls
	});
	echogramCombo.bindStore(echogramStore);

	var echogramDisplayCombo = Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0];
	echogramDisplayStore = new Ext.data.ArrayStore({
		fields: ['image_size'],
		data: [['Echogram Fit', 'Echogram Fit'], ['Echogram Fixed Aspect Ratio', 'Echogram Fixed Aspect Ratio'], ['Echogram Actual Size', 'Echogram Actual Size']]
	});
	echogramDisplayCombo.bindStore(echogramDisplayStore);


	// collapse layer tree
	function collapseTree() {
		var treePanel = Ext.ComponentQuery.query('#antarcticTree')[0];
		treePanel.collapse();
	}

	// collapse menu
	function collapseMenu() {
		var menusPanel = Ext.ComponentQuery.query('menus')[0];
		menusPanel.collapse();
	}

	// render selected line
	function renderLine() {
		antarcticSelectedLine.removeAllFeatures();
		var points = new Array();
		var startX = responseData.data.X[0];
		var startY = responseData.data.Y[0];
		minGps = responseData.data.gps_time[0];
		for (idx = 0; idx <= responseData.data.X.length; idx++) {
			if (!isNaN(responseData.data.X[idx])) {
				if (responseData.data.gps_time[idx] < minGps) {
					minGps = responseData.data.gps_time[idx]
					startX = responseData.data.X[idx]
					startY = responseData.data.Y[idx]
				}
				points.push(new OpenLayers.Geometry.Point(responseData.data.X[idx], responseData.data.Y[idx]))
			}
		};
		var startPoint = new OpenLayers.Geometry.Point(startX, startY);
		var startPointStyle = { fillColor: '#FF0000', pointRadius: 10, strokeColor: '#FF0000' };
		var line = new OpenLayers.Geometry.LineString(points);
		var style = { strokeColor: '#FF0000', strokeWidth: 5 };
		var lineFeature = new OpenLayers.Feature.Vector(line, null, style);
		var pointFeature = new OpenLayers.Feature.Vector(startPoint, null, startPointStyle);
		antarcticSelectedLine.addFeatures([lineFeature, pointFeature]);
		antarcticSelectedLine.redraw(true);
	}

	// render echogram image
	function renderImage() {
		selected_segment = responseData.data.segment_id;
		selected_season = responseData.data.season;
		var cAntarcticImageBrowserPanel = Ext.ComponentQuery.query('#antarcticImageBrowserPanel')[0];
		cAntarcticImageBrowserPanel.removeAll();
		cAntarcticImageBrowserPanel.expand();
		var antarcticEchogramImage = Ext.create('Ext.Img', {
			id: 'antarcticEchogramImage',
			src: responseData.data.echograms[0]
		});
		cAntarcticImageBrowserPanel.add(antarcticEchogramImage);
		copylastSelectedEchogram = responseData.data.echograms[0];
	}

	// execute the selection
	setTimeout(collapseTree, 0);
	setTimeout(collapseMenu, 100);
	setTimeout(renderLine, 500);
	setTimeout(renderImage, 1000);
	setTimeout(Ext.getBody().unmask(), 1000);
	setTimeout(() => antarctic_set_echogram(outEchogramUrls), 1000);
	setTimeout(antarctic_set_display_dropdown, 1000);
};


function antarctic_set_display_dropdown() {
	var callback = {getValue: function () { return Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0].value}};
	Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0].events['select'].listeners[0].fn(callback);
}

function antarctic_set_echogram(outEchogramUrls) {
	if (copylastSelectedEchogram != null) {
		let new_echogram_url = null;
		if (copylastSelectedEchogram.includes('picks')) {
			new_echogram_url = outEchogramUrls[1][0];
			copylastSelectedEchogram = outEchogramUrls[1][0];
		} else if (copylastSelectedEchogram.includes('CSARP_small_jpg')) {
			new_echogram_url = outEchogramUrls[2][0];
			copylastSelectedEchogram = outEchogramUrls[2][0];
		} else {
			new_echogram_url = outEchogramUrls[0][0];
			copylastSelectedEchogram = outEchogramUrls[0][0];
		}

		var cAntarcticImageBrowserPanel = Ext.ComponentQuery.query('#antarcticImageBrowserPanel')[0];
		cAntarcticImageBrowserPanel.removeAll();
		cAntarcticImageBrowserPanel.expand();
		var antarcticEchogramImage = Ext.create('Ext.Img', {
			id: 'antarcticEchogramImage',
			src: new_echogram_url
		});
		cAntarcticImageBrowserPanel.add(antarcticEchogramImage);
	}
}


function incrementUrl(url) {
	const frame_pattern = /((\d+_\d+_)(\d+))/;
	let frame = url.match(frame_pattern)[0];
	let frame_num = parseInt(url.match(frame_pattern)[3]);
	frame_num = Math.min(frame_num + 1, 999);
	frame_num = String(frame_num).padStart(3, '0');

	let frame_full = url.match(frame_pattern)[2] + frame_num;

	const newUrl = url.replace(frame_pattern, frame_full);
	copylastSelectedEchogram = newUrl;
	return newUrl;
}

function decrementUrl(url) {
	const frame_pattern = /((\d+_\d+_)(\d+))/;
	let frame = url.match(frame_pattern)[0];
	let frame_num = parseInt(url.match(frame_pattern)[3]);
	frame_num = Math.max(frame_num - 1, 1);
	frame_num = String(frame_num).padStart(3, '0');

	let frame_full = url.match(frame_pattern)[2] + frame_num;

	const newUrl = url.replace(frame_pattern, frame_full);
	copylastSelectedEchogram = newUrl;
	return newUrl;
}


/*toolbarItems.push(Ext.create('Ext.button.Button', {text:'close echogram browser',handler: antarcticCloseEchogramBrowser,tooltip: "close the echogram browser and go back to the normal interface."}));
toolbarItems.push("-");*/
antarcticMapPanel.addDocked([{
	xtype: 'toolbar',
	dock: 'top',
	items: toolbarItems
}]);

var antarcticStore = Ext.create('Ext.data.TreeStore', {
	model: 'GeoExt.data.LayerTreeModel',
	root: {
		expanded: true,
		children: [
			{
				plugins: [{
					ptype: 'gx_overlaylayercontainer',
					loader: {
						store: antarcticMapPanel.layers
						/*createNode: function(attr) {
							attr.component = {
								xtype: "gx_wmslegend",
								layerRecord: antarcticMapPanel.layers.getByLayer(attr.layer),
								showTitle: false,
								cls: "legend"
							};
							return GeoExt.tree.LayerLoader.prototype.createNode.call(this, attr);
						}*/
					}
				}],
				expanded: false,
				text: 'Data Layers'
			},
			{
				plugins: [{
					ptype: 'gx_baselayercontainer',
					loader: {
						store: antarcticMapPanel.layers,
						createNode: function (attr) {
							attr.component = {
								xtype: "gx_wmslegend",
								layerRecord: antarcticMapPanel.layers.getByLayer(attr.layer),
								showTitle: false,
								cls: "legend"
							};
							return GeoExt.tree.LayerLoader.prototype.createNode.call(this, attr);
						}
					}
				}],
				expanded: false,
				text: 'Reference Layers'
			}
		]
	}
});

function switch_layers(echogramURL, type) {
	const frame_pattern = /(\d+_\d+_\d+)/;
	const season_pattern = /(\d{4}_[a-zA-Z]+(_[a-zA-Z0-9]+)+)/;
	const app_pattern = /\/data\/([a-z]+)\//;

	let frame = echogramURL.match(frame_pattern)[0];
	let season = echogramURL.match(season_pattern)[0];
	let app = echogramURL.match(app_pattern)[1];

	if (type == '1echo.jpg' || type == "2echo_picks.jpg") {
		return "https://data.cresis.ku.edu/data/" + app + "/" + season + "/images/" + frame.substr(0, frame.length - 4) + "/" + frame + "_" + type;
	} else if (type == "CSARP_small_jpg") {
		return "https://data.cresis.ku.edu/data/rds/" + season + "/CSARP_small_jpg/" + frame.substr(0, frame.length - 4) + "/Data_" + frame + ".jpg";
	}

	return null;
}

var antarcticTree = Ext.create('GeoExt.tree.Panel', {
	itemId: 'antarcticTree',
	region: 'east',
	title: 'Map Layer Selection',
	width: 250,
	collapsible: true,
	autoScroll: true,
	store: antarcticStore,
	rootVisible: false,
	lines: true
});

var antarcticEchogramPanel = Ext.create('Ext.Panel', {
	layout: 'border',
	itemId: 'antarcticImageBrowserPanel',
	defaults: {
		collapsible: true,
		bodyStyle: 'padding:0px'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [{
				xtype: 'button',
				itemId: 'antarcticEchogramNotice',
				text: 'DOI/Metadata',
				scale: 'small',
				flex: 1.5,
				handler: function () {
					let selectedSystem = Ext.ComponentQuery.query('#selectedSystem')[0].value;
					// let notice = "<i>Note: Echogram images are static previews and may not represent the most recent data. They should be used for reference only.</i>";
					Ext.Ajax.request({
						type: "POST",
						url: '/ops/get/segment/metadata',
						params: { app: selectedSystem, data: JSON.stringify({ properties: { segment_id: selected_segment } }) },
						success: function (response) {
							let response_data = JSON.parse(response.responseText);
							let dois = response_data['data']['dois'].flatMap((d) => '<a target="_blank" href="https://doi.org/' + d + '">' + d + '</a>').join(', ');
							let funding_sources = response_data['data']['funding_sources'].join(', ');
							let rors = response_data['data']['rors'].flatMap((r) => '<a target="_blank" href="https://ror.org/' + r + '">' + r + '</a>').join(', ');

							let contact_info = "Contact opr@openpolarradar.org";
							let message = "Digital Object Identifier (DOI): ";
							if (response_data['data']['dois'].length != 0) {
								message = message + dois;
							} else {
								message = message + contact_info;
							}
							message = message + "<br/>Funding Sources: ";
							if (response_data['data']['funding_sources'].length != 0) {
								message = message + funding_sources;
							} else {
								message = message + contact_info;
							}
							message = message + "<br/>Research Organization Registry (ROR): ";
							if (response_data['data']['rors'].length != 0) {
								message = message + rors;
							} else {
								message = message + contact_info;
							}

							Ext.Msg.alert("Segment " + response_data['data']['segment_name'], message + "<br/>&nbsp;<br/>");
						},
						error: function () {
							Ext.Msg.alert('ERROR', 'Could not get metadata.\n');
						}
					});
				}
			}, {
				xtype: 'button',
				itemId: 'antarcticLinkButton',
				text: 'Access Data',
				scale: 'small',
				flex: 1,
				handler: function () {
					var echogramCombo = Ext.ComponentQuery.query('#antarcticEchogramSelector')[0];
					const pattern = /(\d+_\d+_\d+)(?=_\d+echo)/;

					// Presently a bit of a hacky method of producing the urls by altering the url to the echogram image
					// Also uses global variables that could probably be replaced with a better solution.
					let url = Ext.ComponentQuery.query('#antarcticEchogramImage')[0].src;
					let frame = Ext.ComponentQuery.query('#antarcticEchogramImage')[0].src.match(pattern)[0];

					let qlook_url = url.replace("images", "CSARP_qlook").split("/").slice(0, -1).join("/");
					let qlook_frm_url = qlook_url + "/" + "Data_" + frame + ".mat";

					let standard_url = url.replace("images", "CSARP_standard").split("/").slice(0, -1).join("/")
					let standard_frm_url = standard_url + "/" + "Data_" + frame + ".mat";

					let layer_url = url.replace("images", "csv").split("/").slice(0, -2).join("/") + "/" + selected_season + ".csv";

					Ext.Msg.alert("Access Data", "<a target='_blank' href='" + qlook_url + "'>qlook (unfocused SAR) full segment directory</a><br><a target='_blank' href='" + qlook_frm_url + "'>qlook (unfocused SAR) this frame only</a><br><a target='_blank' href='" + standard_url + "'>standard (focused SAR) full segment directory</a><br><a target='_blank' href='" + standard_frm_url + "'>standard (focused SAR) this frame only</a><br><a target='_blank' href='" + layer_url + "'>layer data for entire season</a>");
				}

			}, {
				xtype: 'combo',
				editable: false,
				itemId: 'antarcticEchogramSelector',
				queryMode: 'local',
				displayField: 'echogram_name',
				valueField: 'echogram_url',
				emptyText: "Echogram w/o layers",
				flex: 2.5,
				listeners: {
					'select': function (combo, records) {
						var cantarcticImageBrowserPanel = Ext.ComponentQuery.query('#antarcticImageBrowserPanel')[0];
						cantarcticImageBrowserPanel.removeAll();
						cantarcticImageBrowserPanel.expand();
						if (records.length > 0) {
							if (records[0].data.echogram_url.includes('picks')) {
								copylastSelectedEchogram = switch_layers(copylastSelectedEchogram, '2echo_picks.jpg');
							} else if (records[0].data.echogram_url.includes('CSARP_small_jpg')) {
								copylastSelectedEchogram = switch_layers(copylastSelectedEchogram, 'CSARP_small_jpg');
							} else {
								copylastSelectedEchogram = switch_layers(copylastSelectedEchogram, '1echo.jpg');
							}
						}

						var antarcticEchogramImage = Ext.create('Ext.Img', {
							id: 'antarcticEchogramImage',
							src: copylastSelectedEchogram
						});
						cantarcticImageBrowserPanel.add(antarcticEchogramImage);

						var callback = {getValue: function () { return Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0].value}};
						Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0].events['select'].listeners[0].fn(callback);
					}
				}
			}, {
				xtype: 'combo',
				editable: false,
				itemId: 'antarcticDisplayModeSelector',
				queryMode: 'local',
				displayField: 'image_size',
				valueField: 'image_size',
				emptyText: 'Echogram Fit',
				flex: 2.5,
				listeners: {
					'select': function (combo, records) {
						if (combo.getValue() == 'Echogram Fit') {
							var echogramImage = Ext.getCmp('antarcticEchogramImage');
							echogramImage.el.setStyle({
								width: '100%',
								height: '100%',
								'max-width': '100%',
								'max-height': '100%'
							});
							selectedDisplay = 0;
						} else if (combo.getValue() == 'Echogram Fixed Aspect Ratio') {
							var echogramImage = Ext.getCmp('antarcticEchogramImage');
							echogramImage.el.setStyle({
								width: '100%',
								height: 'auto',
								'max-width': '100%',
								'max-height': 'none'
							});
							selectedDisplay = 1;
						} else if (combo.getValue() == 'Echogram Actual Size') {
							var echogramImage = Ext.getCmp('antarcticEchogramImage');
							echogramImage.el.setStyle({
								width: 'auto',
								height: 'auto',
								'max-width': 'none',
								'max-height': 'none'
							});
							selectedDisplay = 2;
						}
					}
				}

			},  {
				xtype: 'button',
				text: 'Previous',
				itemId: 'antarcticPrevButton',
				handler: function () {
					if (copylastSelectedEchogram) {
						decrementUrl(copylastSelectedEchogram);
						var echogramImage = Ext.ComponentQuery.query('#antarcticEchogramImage')[0];
						if (echogramImage) {
							echogramImage.setSrc(copylastSelectedEchogram);
						}
					}
				}

			},
			{
				xtype: 'button',
				text: 'Next',
				itemId: 'antarcticNextButton',
				handler: function () {
					if (copylastSelectedEchogram) {
						incrementUrl(copylastSelectedEchogram);

						var echogramImage = Ext.ComponentQuery.query('#antarcticEchogramImage')[0];
						if (echogramImage) {
							echogramImage.setSrc(copylastSelectedEchogram);
						}
					}
				}
			}, {
				xtype: 'splitbutton',
				text: 'Window Width',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: '25%',
							handler: function () {
								antarcticEchogramPanel.setWidth(Ext.getBody().getViewSize().width * 0.3);
							}
						},
						{
							text: '50%',
							handler: function () {
								antarcticEchogramPanel.setWidth(Ext.getBody().getViewSize().width * 0.48);
							}
						},
						{
							text: '75%',
							handler: function () {
								antarcticEchogramPanel.setWidth(Ext.getBody().getViewSize().width * 0.64);
							}
						},
						{
							text: '100%',
							handler: function () {
								antarcticEchogramPanel.setWidth(Ext.getBody().getViewSize().width * 0.955);
							}
						}
					]
				}),
				handler: function () {
				}
			},]

	}],
	region: 'west',
	title: 'Echogram Image Browser',
	collapsible: true,
	collapsed: true,
	autoScroll: true,
	resizable: {
		handles: 'e',
		pinned: true,
		dynamic: true
	},
	listeners: {
		resize: function() {
			setTimeout(function () {
				var callback = {getValue: function () { return Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0].value}};
				Ext.ComponentQuery.query('#antarcticDisplayModeSelector')[0].events['select'].listeners[0].fn(callback);
			}, 300);
		}
	},
	width: 797
});

Ext.define('OPS.view.MapTabs.AntarcticTab.AntarcticTab', {

	extend: 'Ext.Panel',

	layout: 'border',
	defaults: {
		collapsible: false,
		bodyStyle: 'padding:0px'
	},

	alias: 'widget.antarctictab',
	title: 'Antarctic',

	items: [antarcticEchogramPanel, antarcticMapPanel, antarcticTree]
});
